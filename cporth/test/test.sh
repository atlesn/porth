#!/bin/bash

TMPDIR=tmp
CPORTH=../src/cporth

err() {
	MSG=$1
	echo "[ERROR] $MSG" 1>&2
	exit 1
}

fail() {
	MSG=$1
	echo "[FAIL] $MSG" 1>&2

	echo "[STDOUT]"
	cat $TMPDIR/stdout

	if test -f $TMPDIR/diff; then
		echo "[DIFF]"
		cat $TMPDIR/diff
	fi

	echo "[STDERR]"
	cat $TMPDIR/stderr

	exit 2
}

pass() {
	echo "[PASS]" 1>&2
	exit 0
}

if [ "x$1" = "x" ]; then
	err "Usage: $0 {TEST NAME}"
fi

PROGRAM="$1.porth"
TESTSPEC="$1.txt"

B_BUF_TMP=$TMPDIR/parse.buf.tmp
B_BUF_PREFIX=$TMPDIR/parse.buf.

mkdir -p $TMPDIR || exit 1
rm -f $TMPDIR/* > /dev/null 2>&1

if ! [ -f $PROGRAM ]; then
	err "File $PROGRAM not found"
fi

if ! [ -f $TESTSPEC ]; then
	err "File $TESTSPEC not found"
fi

split() {
	CMD=$1
	ARG=$2
	VAL=$3
}

filesize() {
	wc -c $1 | awk '{print $1;}'
}

args=
b_size_target=
b_first=0
let i=1
while IFS= read -r line; do
	if [ "x$b_arg" != "x" ]; then
		file=$B_BUF_PREFIX$b_arg
		size=`filesize $file`
		if [ $size -lt $b_size_target ]; then
			if [ $b_first -eq 0 ]; then
				/bin/echo >> $file
			fi
			/bin/echo -n "$line" >> $file
			b_first=0
		fi

		size=`filesize $file`
		if [ $size -gt $b_size_target ]; then
			err "Too many bytes for variable $b_arg"
		elif [ $size -eq $b_size_target ]; then
			b_arg=
		fi
	elif [ "x$line" != "x" ]; then
		split $line
		argname=arg_$ARG
		args="$args $ARG"

		if [ "x$CMD" = "x:i" ]; then
			eval $argname="$VAL"
		elif [ "x$CMD" = "x:b" ]; then
			b_arg=$ARG
			b_size_target=$VAL
			b_first=1
			file=$B_BUF_PREFIX$b_arg
			rm -f $file || exit 1
			touch $file || exit 1
		else
			err "Unknown data type in $TESTSPEC:$i"
		fi
	fi

	let i=$i+1
done < "$TESTSPEC"

getarg() {
	argname=arg_$1
	file=$B_BUF_PREFIX$1
	if [ -f $file ]; then
		cat $file
	elif [ "x${!argname}" != "x" ]; then
		echo ${!argname}
	else
		err "Required variable $1 not found in $TESTSPEC"
	fi
}

argc=`getarg argc`
argv=

let i=0
while [ $i -lt $argc ]; do
	arg=`getarg arg$i`
	argv="$argv $arg"
	let i=$i+1
done

echo "[RUN] $CPORTH $PROGRAM $argv"
$CPORTH $PROGRAM $argv > $TMPDIR/stdout 2> $TMPDIR/stderr
ret=$?
ret_exp=`getarg returncode`

if [ "x$ret" != "x$ret_exp" ]; then
	fail "Return code mismatch $ret<>$ret_exp"
fi

if ! diff ${B_BUF_PREFIX}stdout $TMPDIR/stdout > $TMPDIR/diff; then
	fail "stdout mismatch"
fi


pass
