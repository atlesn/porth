#!/bin/bash

DIR=tests
SUFFIX=.txt

ret=0

for testname in `ls $DIR/*$SUFFIX | sed -e "s/$DIR\///" -e "s/$SUFFIX$//"`; do
	echo "[TEST] $testname"
	spec=$DIR/$testname$SUFFIX
	prog=$DIR/$testname.porth

	if ! ./test.sh $DIR/$testname; then
		ret=1
	fi

	echo "[DONE]"
done

exit $ret
