#ifndef CPORTH_LOCATION_H
#define CPORTH_LOCATION_H

#include <stddef.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include <sys/types.h>

typedef enum cporth_location_note {
	CPORTH_LOC_NOTE_EMPTY,
	CPORTH_LOC_NOTE_MACRO_EXPANSION,
	CPORTH_LOC_NOTE_FILE_INCLUSION
} cporth_location_note;

#define CPORTH_LOC_NOTE_MAX CPORTH_LOC_NOTE_FILE_INCLUSION

extern const char * const cporth_location_notes[];

typedef struct cporth_location cporth_location;

typedef struct cporth_location_ref {
	size_t i;
} cporth_location_ref;

typedef struct cporth_location {
	cporth_location_ref parent_ref;
	const char *filename;
	const char *buf;
	cporth_location_note note;
	size_t length;
	size_t pos;
	size_t col_pos;
	size_t row_pos;
	void *ctx;
} cporth_location;

static inline const char *cporth_location_note_to_str (
		cporth_location_note note
) {
	assert(note <= CPORTH_LOC_NOTE_MAX);
	return cporth_location_notes[note];
}

static inline void cporth_location_init (
		cporth_location *loc,
		const char *filename,
		cporth_location_note note
) {
	memset(loc, '\0', sizeof(*loc));
	loc->filename = filename;
	loc->note = note;
	loc->col_pos = 1;
	loc->row_pos = 1;
}

static inline void cporth_location_set_parent (
		cporth_location *loc,
		const cporth_location_ref locr
) {
	loc->parent_ref = locr;
}

static inline int cporth_location_compare (
		const cporth_location *a,
		const cporth_location *b
) {
	return memcmp(a, b, sizeof(*a));
}

#endif /* CPORTH_LOCATION_H */
