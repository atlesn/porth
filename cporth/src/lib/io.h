#ifndef CPORTH_IO_H
#define CPORTH_IO_H

#include <stddef.h>

int cporth_io_read_file (
		void **data,
		size_t *length,
		const char *filename
);

#endif /* CPORTH_IO_H */
