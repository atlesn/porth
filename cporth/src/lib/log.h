#ifndef CPORTH_LOG_H
#define CPORTH_LOG_H

#include <stdio.h>

#define cporth_log(...) \
	printf("[INFO] " __VA_ARGS__)

#define cporth_err(...) \
	fprintf(stderr, "[ERROR] " __VA_ARGS__)

#endif /* CPORTH_LOG_H */
