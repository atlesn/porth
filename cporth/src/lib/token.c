#include <assert.h>

#include "token.h"
#include "allocate.h"
#include "log.h"
#include "error.h"
#include "util.h"

int cporth_token_set (
		cporth_token *token,
		cporth_token_type type,
		const cporth_keyword *kw,
		const void *ptr,
		size_t size
) {
	int ret = CPORTH_OK;

	CPORTH_FREE_IF_NOT_NULL(token->ptr);

	// All token data is padded with \0
	const size_t alloc_size = size + 1;

	if ((token->ptr = cporth_allocate_zero(alloc_size)) == NULL) {
		cporth_err("Failed to allocate memory for data in %s\n", __func__);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	if (ptr != NULL && size > 0) {
		memcpy(token->ptr, ptr, size);
	}

	*((char *) (token->ptr + size)) = '\0';

	token->type = type;
	token->size = size;
	token->kw = kw;

	out:
	return ret;
}

int cporth_token_init (
		cporth_token *token,
		cporth_token_type type,
		const void *ptr,
		size_t size
) {
	memset(token, '\0', sizeof(*token));

	return cporth_token_set(token, type, NULL, ptr, size);
}

void cporth_token_cleanup (
		cporth_token *token
) {
	CPORTH_FREE_IF_NOT_NULL(token->ptr);
}
