#ifndef CPORTH_PARSE_H
#define CPORTH_PARSE_H

typedef struct cporth_program cporth_program;

void cporth_parse_operator (
		int *found,
		cporth_token *t
);
int cporth_parse (
		cporth_program *program
);

#endif /* CPORTH_PARSE_H */
