#include "location.h"
#include "log.h"

// Must match enum in location.h
const char * const cporth_location_notes[] = {
	"",
	"expanded macro",
	"included file"
};
