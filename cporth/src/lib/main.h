#ifndef CPORTH_MAIN_H
#define CPORTH_MAIN_H

typedef struct cporth_program cporth_program;

int cporth_main_file_to_program (
		const char *filename,
		int (*callback)(const cporth_program *program, void *arg),
		void *arg
);

#endif /* CPORTH_MAIN_H */
