#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "program.h"
#include "program_struct.h"
#include "operator.h"
#include "error.h"
#include "log.h"
#include "allocate.h"
#include "location.h"
#include "util.h"
#include "token.h"

#define CPORTH_PROGRAM_ALLOCATE_STEP 256

typedef enum cporth_frame_type {
	CPORTH_FRAME_OPERAND,
	CPORTH_FRAME_OPERATOR
} cporth_frame_type;

int cporth_program_new (
		cporth_program **program
) {
	int ret = CPORTH_OK;

	*program = NULL;

	cporth_program *program_tmp = NULL;

	if ((program_tmp = cporth_allocate_zero(sizeof(*program_tmp))) == NULL) {
		cporth_err("Could not allocate memory for program in %s\n", __func__);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	*program = program_tmp;
	program_tmp = NULL;

	goto out;
//	out_free:
//	cporth_free(program_tmp);
	out:
	return ret;
}

void cporth_program_destroy (
		cporth_program *program
) {
	for (size_t i = 0; i < program->tokens_length; i++) {
		cporth_token_cleanup(program->tokens + i);
	}
	CPORTH_FREE_IF_NOT_NULL(program->tokens);
	CPORTH_FREE_IF_NOT_NULL(program->locations);
	cporth_free(program);
}

void cporth_program_tokens_trim (
		cporth_program *program,
		cporth_token_ref last
) {
	const cporth_token *last_t = CPORTH_PROGRAM_REF_TO_TOKEN(last);

	assert(last_t >= program->tokens && last_t < program->tokens + program->tokens_length);

	for (const cporth_token *t = program->tokens + program->tokens_length - 1; t >= last_t; t--) {
		CPORTH_PROGRAM_TOKEN_POP();
	}
}

int cporth_program_token_push (
		cporth_program *program,
		cporth_token_type id,
		const void *ptr,
		size_t size,
		const cporth_location loc,
		int (*postprocess)(CPORTH_PROGRAM_KW_POSTPROCESS_ARGS),
		const cporth_keyword *kw,
		void *arg
) {
	int ret = CPORTH_OK;

	if (program->tokens_length == program->tokens_size) {
		program->tokens_size += CPORTH_PROGRAM_ALLOCATE_STEP;

		// Expand tokens
		cporth_token *tokens_new = NULL;
		if ((tokens_new = cporth_reallocate(program->tokens, sizeof(*program->tokens) * program->tokens_size)) == NULL) {
			cporth_err("Failed to expand token memory to %lu in %s\n", program->tokens_size, __func__);
			ret = CPORTH_ERR_HARD;
			goto out;
		}

		// Expand locations
		cporth_location *locations_new = NULL;
		if ((locations_new = cporth_reallocate(program->locations, sizeof(*program->locations) * program->tokens_size)) == NULL) {
			cporth_err("Failed to expand location memory to %lu in %s\n", program->tokens_size, __func__);
			ret = CPORTH_ERR_HARD;
			goto out;
		}

		program->tokens = tokens_new;
		program->locations = locations_new;
	}

	memcpy(program->locations + program->tokens_length, &loc, sizeof(loc));

	if ((ret = cporth_token_init(&program->tokens[program->tokens_length++], id, ptr, size)) != 0) {
		goto out;
	}

	if (postprocess != NULL && (ret = postprocess(program->tokens + program->tokens_length - 1, kw, arg)) != 0) {
		goto out;
	}

	out:
	return ret;
}

static int __cporth_program_token_copy_and_push_postprocess (CPORTH_PROGRAM_KW_POSTPROCESS_ARGS) {
	(void)(kw);

	const cporth_token *token_orig = arg;

	token->kw = token_orig->kw;
	token->jump = token_orig->jump;

	return CPORTH_OK;
}

int cporth_program_token_copy_and_push (
		cporth_program *program,
		const cporth_token_ref tref,
		const cporth_location *loc
) {
	int ret = CPORTH_OK;

	cporth_location loc_new = *loc;
	CPORTH_PROGRAM_LOCATION_PARENT_SET_FROM_REF(loc_new, tref);

	const cporth_token token = *(CPORTH_PROGRAM_REF_TO_TOKEN(tref));

	if ((ret = cporth_program_token_push (
			program,
			token.type,
			token.ptr,
			token.size,
			loc_new,
			__cporth_program_token_copy_and_push_postprocess,
			NULL,
			(void *) &token
	)) != 0) {
		goto out;
	}

	out:
	return ret;
}

void cporth_program_dump (
		const cporth_program *program
) {
	printf("// -------- Dump program begin");
	const char *filename_prev = NULL;
	size_t row_pos_prev = 0;
	for (size_t i = 0; i < program->tokens_length; i++) {
		const cporth_token *token = program->tokens + i;
		const cporth_location *loc = program->locations + i;
		const char *filename = loc->filename;

		if (row_pos_prev != loc->row_pos) {
			printf("\n");
		}
		row_pos_prev = loc->row_pos;

		if (filename_prev != filename) {
			printf("\n// ---- File %s\n", filename);
		}
		filename_prev = filename;

		switch (token->type) {
			case CPORTH_TP_WORD:
				printf("%s ", (const char *) token->ptr);
				break;
			case CPORTH_TP_KEYWORD:
				printf("%s ", (const char *) token->ptr);
				if (CPORTH_TOKEN_JUMPOF(token) == NULL && token->kw->id == CPORTH_KW_MEMORY) {
					// Skip the memory pointer
					i++;
				}
				break;
			case CPORTH_TP_CHAR:
				printf("'%s' ", (const char *) token->ptr);
				break;
			case CPORTH_TP_STR:
				printf("\"%s\" ", (const char *) token->ptr);
				break;
			case CPORTH_TP_CSTR:
				printf("\"%s\"c ", (const char *) token->ptr);
				break;
			case CPORTH_TP_INT:
				printf("%" PRIi64 " ", *((int64_t *) token->ptr));
				break;
			case CPORTH_TP_BOOL:
				printf("%" PRIu64 " cast(bool) ", *((uint64_t *) token->ptr));
				break;
			case CPORTH_TP_PTR:
				printf("%" PRIu64 " cast(ptr) ", *((uint64_t *) token->ptr));
				break;
			case CPORTH_TP_CALL:
				printf("%s ", token->kw->name);
		};
	}
	printf("\n// -------- Dump program end\n");
}

static inline cporth_location_ref __cporth_program_token_to_lref (
		const cporth_program *program,
		const cporth_token *token
) {
	cporth_location_ref ret = {
		((const void *) token - (const void *) program->tokens) / sizeof(*token)
	};
	return ret;
}

int cporth_program_here_str_l (
		char **result,
		const cporth_location *loc
) {
	*result = NULL;
	return cporth_asprintf(result, "%s:%lu:%lu", loc->filename, loc->row_pos, loc->col_pos);
}

void cporth_program_err_custom_l (
		const cporth_location *loc,
		const char *str
) {
	if (loc->note != CPORTH_LOC_NOTE_EMPTY) {
		cporth_err("%s %s %s:%lu:%lu\n", str, cporth_location_note_to_str(loc->note), loc->filename, loc->row_pos, loc->col_pos);
	}
	else {
		cporth_err("%s %s:%lu:%lu\n", str, loc->filename, loc->row_pos, loc->col_pos);
	}
}

void cporth_program_err_l (
		const cporth_location *t
) {
	cporth_program_err_custom_l(t, "In");
}

void cporth_program_err_custom (
		const cporth_program *program,
		const cporth_location_ref lref,
		const char *str
) {
	cporth_program_err_custom_l (
		CPORTH_PROGRAM_REF_TO_LOCATION(lref),
		str
	);
}

void cporth_program_err (
		const cporth_program *program,
		const cporth_location_ref lref
) {
	cporth_program_err_l (
		CPORTH_PROGRAM_REF_TO_LOCATION(lref)
	);
}

void cporth_program_err_descend (
		const cporth_program *program,
		const cporth_location_ref lref
) {
	cporth_program_err(program, lref);

	const struct cporth_location *loc = CPORTH_PROGRAM_REF_TO_LOCATION(lref);
	const cporth_location *loc_parent = program->locations + loc->parent_ref.i;
	if (loc != loc_parent) {
		cporth_program_err_descend(program, loc->parent_ref);
	}
}

void cporth_program_err_custom_t (
		const cporth_program *program,
		const cporth_token *token,
		const char *str
) {
	return cporth_program_err_custom(program, __cporth_program_token_to_lref(program, token), str);
}

void cporth_program_err_t (
		const cporth_program *program,
		const cporth_token *token
) {
	return cporth_program_err(program, __cporth_program_token_to_lref(program, token));
}

void cporth_program_err_descend_t (
		const cporth_program *program,
		const cporth_token *token
) {
	return cporth_program_err_descend(program, __cporth_program_token_to_lref(program, token));
}

size_t cporth_program_nesting_count (
		const cporth_program *program,
		const cporth_location_ref lref
) {
	size_t count = 0;

	cporth_location_ref i = lref;
	cporth_location_ref prev_i = {0};
	while (i.i != prev_i.i && i.i != 0) {
		prev_i = i;
		i = CPORTH_PROGRAM_REF_TO_LOCATION(lref)->parent_ref;
		count++;
	}
	return count;
}
