#include <string.h>

#include "operator.h"

const char * const cporth_operator_names[] = {
	"+",
	"-",
	"*",
	"divmod",
	"max",

	"print",

	"=",
	">",
	"<",
	">=",
	"<=",
	"!=",

	"shr",
	"shl",
	"or",
	"and",
	"not",

	"dup",
	"swap",
	"drop",
	"over",
	"rot",

	"!8",
	"@8",
	"!16",
	"@16",
	"!32",
	"@32",
	"!64",
	"@64",

	"cast(ptr)",
	"cast(int)",
	"cast(bool)",

	"argc",
	"argv",
	"envp",

	"syscall0",
	"syscall1",
	"syscall2",
	"syscall3",
	"syscall4",
	"syscall5",
	"syscall6",

	"offset",
	"reset",

	"dbg-stackdump"
};
	
void cporth_operator_lookup (int *found, cporth_operator *op_result, const char *name) {
	*found = 0;
	*op_result = 0;

	for (cporth_operator op = 0; op < CPORTH_OP_NUMBER_OF; op++) {
		if (strcmp(cporth_operator_names[op], name) == 0) {
			*found = 1;
			*op_result = op;
			return;
		}
	}

	return;
}
