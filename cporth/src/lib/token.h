#ifndef CPORTH_TOKEN_H
#define CPORTH_TOKEN_H

#include <stddef.h>
#include <sys/types.h>

#include "operator.h"
#include "analyze_data.h"
#include "dict.h"

typedef enum cporth_keyword_type {
	CPORTH_KW_IF,
	CPORTH_KW_IFSTAR,
	CPORTH_KW_ELSE,
	CPORTH_KW_END,
	CPORTH_KW_WHILE,
	CPORTH_KW_DO,
	CPORTH_KW_HERE,
	CPORTH_KW_MACRO,
	CPORTH_KW_INCLUDE,
	CPORTH_KW_MEMORY,
	CPORTH_KW_PROC,
	CPORTH_KW_CONST,
	CPORTH_KW_ASSERT,
	CPORTH_KW_INT,
	CPORTH_KW_PTR,
	CPORTH_KW_BOOL,
	CPORTH_KW_BIKESHEDDER,
	CPORTH_KW_IN,
	/* cporth-specific keywords */
	CPORTH_KW_CP_STRUCT,
	CPORTH_KW_CP_VAR,
	/* Special keywords changed during parsing */
	CPORTH_KW_SP_ORELSE,   // ELSE with IFSTAR changes to ORELSE
	/* OFFSET and RESET are operators in cporth */
} cporth_keyword_type;

#define CPORTH_SCAN_KW_ARGS              \
    const cporth_keyword *kw,            \
    void *arg

typedef struct cporth_token_ref {
	size_t i;
} cporth_token_ref;

typedef struct cporth_token_diff {
	ssize_t i;
} cporth_token_diff;

typedef struct cporth_token cporth_token;
typedef struct cporth_keyword cporth_keyword;

typedef struct cporth_keyword {
	cporth_keyword_type id;
	const char *name;
	int (*scan)(CPORTH_SCAN_KW_ARGS);
	cporth_token_ref ref;
	void *memory;
	int64_t constant;
	cporth_analyze_data data;
	cporth_dict dict;
} cporth_keyword;

typedef enum cporth_token_type {
	CPORTH_TP_INT,
	CPORTH_TP_BOOL,
	CPORTH_TP_PTR,
	CPORTH_TP_WORD,
	CPORTH_TP_STR,
	CPORTH_TP_CSTR,
	CPORTH_TP_CHAR,
	CPORTH_TP_KEYWORD,
	CPORTH_TP_CALL
} cporth_token_type;

typedef struct cporth_token {
	cporth_token_type type;
	cporth_operator op;
	const cporth_keyword *kw;
	cporth_token_diff jump;
	short jump_return;
	void *ptr;
	size_t size;
} cporth_token;

static inline void cporth_token_jump_set (
		cporth_token *token,
		const cporth_token *target
) {
	token->jump.i = target - token;
}

static inline int cporth_token_is_kw_type (
		const cporth_token *token,
		cporth_keyword_type type
) {
	return (token->kw != NULL && token->kw->id == type ? 1 : 0);
}

#define CPORTH_TOKEN_JUMPOF(t) \
    ((t)->jump.i == 0 ? NULL : (t) + (t)->jump.i)

#define CPORTH_TOKEN_JUMPOF_OR_SELF(t) \
    ((t)->jump.i == 0 ? (t) : (t) + (t)->jump.i)

#define CPORTH_TOKEN_RETURNOF(t) \
    ((t)->jump_return)

#define CPORTH_TOKEN_JUMP_SET(t,t_jump) \
    cporth_token_jump_set(t, t_jump)

#define CPORTH_TOKEN_JUMP_UNSET(t) \
    t->jump.i = 0

#define CPORTH_TOKEN_JUMP_SET_FROM(target,source) \
    target->jump = source->jump

int cporth_token_set (
		cporth_token *token,
		cporth_token_type type,
		const cporth_keyword *kw,
		const void *ptr,
		size_t size
);
int cporth_token_init (
		cporth_token *token,
		cporth_token_type type,
		const void *ptr,
		size_t size
);
void cporth_token_cleanup (
		cporth_token *token
);

#endif /* CPORTH_TOKEN_H */
