#include "file.h"
#include "error.h"
#include "allocate.h"
#include "log.h"
#include "io.h"
#include "util.h"
#include "location.h"

typedef struct cporth_file {
	const char *filename;
	void *buf;
	size_t length;
} cporth_file;

static int __cporth_file_new (
		cporth_file **file,
		const char *filename,
		void **buf,
		size_t length
) {
	int ret = CPORTH_OK;

	*file = NULL;

	cporth_file *file_tmp = NULL;

	if ((file_tmp = cporth_allocate_zero(sizeof(*file_tmp))) == NULL) {
		cporth_err("Failed to allocate memory in %s\n", __func__);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	file_tmp->filename = filename;
	file_tmp->length = length;
	file_tmp->buf = *buf;
	*buf = NULL;

	*file = file_tmp;

	goto out;
//	out_free:
//		cporth_free(file_tmp);
	out:
		return ret;
}

int cporth_file_new (
		cporth_file **file,
		const char *filename
) {
	int ret = CPORTH_OK;

	void *buf = NULL;
	size_t length = 0;
	cporth_file *file_tmp = NULL;

	if ((ret = cporth_io_read_file(&buf, &length, filename)) != 0) {
		goto out;
	}

	if ((ret = __cporth_file_new (&file_tmp, filename, &buf, length)) != 0) {
		goto out;
	}

	*file = file_tmp;

	out:
	CPORTH_FREE_IF_NOT_NULL(buf);
	return ret;
}

void cporth_file_destroy (
		cporth_file *file
) {
	cporth_free(file->buf);
	cporth_free(file);
}

void cporth_file_location_buf_set (
		cporth_location *loc,
		const cporth_file *file
) {
	loc->buf = file->buf;
	loc->length = file->length;
}
