#ifndef CPORTH_LEX_H
#define CPORTH_LEX_H

#include <stddef.h>

typedef struct cporth_dict cporth_dict;
typedef struct cporth_dict_entry cporth_dict_entry;
typedef struct cporth_keyword cporth_keyword;

typedef struct cporth_dict_entry {
	cporth_dict_entry *next;
	const cporth_keyword *kw;
	size_t memory_size;
	size_t memory_pos;
} cporth_dict_entry;

typedef struct cporth_dict {
	const cporth_dict *parent;
	cporth_dict_entry *first;
	cporth_dict_entry *last;
} cporth_dict;

typedef struct cporth_dict cporth_dict;
typedef struct cporth_keyword cporth_keyword;

void cporth_dict_private_clear (cporth_dict *dict);
const cporth_dict_entry *cporth_dict_lookup (const cporth_dict *dict, const char *name);
const cporth_keyword *cporth_dict_kw_lookup (const cporth_dict *dict, const char *name);
const cporth_dict_entry *cporth_dict_lookup_no_parent (const cporth_dict *dict, const char *name);
int cporth_dict_kw_push_global (const cporth_keyword *kw);
int cporth_dict_kw_push_dynamic_unchecked (
		cporth_dict *target_dict,
		const cporth_keyword *kw,
		size_t memory_size,
		size_t memory_pos,
		const cporth_dict *kw_dict
);
int cporth_dict_kw_push_dynamic (
		cporth_dict *target_dict,
		const cporth_keyword *kw,
		size_t memory_size,
		size_t memory_pos,
		const cporth_dict *kw_dict
);
int cporth_dict_kw_update_dynamic (
		cporth_dict *target_dict,
		const char *name,
		size_t memory_size
);
void cporth_dict_cleanup (void);

#endif /* CPORTH_LEX_H */
