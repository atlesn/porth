#include <assert.h>

#include "analyze.h"
#include "program.h"
#include "program_struct.h"
#include "scan_ctx.h"
#include "location.h"
#include "token.h"
#include "error.h"
#include "log.h"
#include "allocate.h"

#define CPORTH_ANALYZE_DATA_MAX CPORTH_ANALYZE_DATA_PTR
#define CPORTH_ANALYZE_STACK_EXTRA 16

const char * const cporth_analyze_data_names[] = {
	"ANY",
	"INT",
	"BOOL",
	"PTR",
	"GENERIC"
};

typedef enum cporth_analyze_flags {
	CPORTH_ANALYZE_FLAGS_NONE
} cporth_analyze_flags;

typedef struct cporth_analyze_frame {
	cporth_analyze_data data;
	cporth_analyze_flags flags;
	const cporth_keyword *kw;
	const char *generic;
} cporth_analyze_frame;

typedef struct cporth_analyze_stack {
	size_t length;
	size_t size;
	cporth_analyze_frame frames[1];
} cporth_analyze_stack;

static inline int __cporth_analyze_stack_new (
		cporth_analyze_stack **stack,
		const cporth_program *program
) {
	int ret = CPORTH_OK;

	*stack = NULL;

	cporth_analyze_stack *stack_tmp = NULL;

	if ((stack_tmp = cporth_allocate(sizeof(*stack_tmp) - sizeof(stack_tmp->frames) + (program->tokens_length + CPORTH_ANALYZE_STACK_EXTRA) * sizeof(stack_tmp->frames))) == NULL) {
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	memset(stack_tmp, '\0', sizeof(*stack_tmp));

	stack_tmp->size = program->tokens_size;

	*stack = stack_tmp;

	out:
	return ret;
}

static inline void __cporth_analyze_stack_destroy (
		cporth_analyze_stack *stack
) {
	cporth_free(stack);
}

static inline void __cporth_analyze_stack_push (
		cporth_analyze_stack *stack,
		cporth_analyze_data type,
		const cporth_keyword *kw,
		const char *generic
) {
	assert(stack->length < stack->size);
	cporth_analyze_frame frame = { type, 0, kw, generic};
	stack->frames[stack->length++] = frame;
}

static inline void __cporth_analyze_stack_push_copy (
		cporth_analyze_stack *stack,
		cporth_analyze_frame frame
) {
	assert(stack->length < stack->size);
	stack->frames[stack->length++] = frame;
}

static inline void __cporth_analyze_stack_pop_n (
		cporth_analyze_stack *stack,
		size_t n
) {
	assert(stack->length >= n);
	stack->length -= n;
}

#define ATFULL(s,n) \
    (**(&s->frames + (s->length - n)))

#define AT_DATA(s,n) \
    (ATFULL(s,n).data)

#define TOP \
    AT_DATA(stack, 1)

#define TOPTOP \
    AT_DATA(stack, 2)

#define TOP3 \
    AT_DATA(stack, 3)

#define TOP4 \
    AT_DATA(stack, 4)

#define TOP_FULL \
    ATFULL(stack, 1)

#define TOPTOP_FULL \
    ATFULL(stack, 2)

#define TOP3_FULL \
    ATFULL(stack, 3)

#define TOP4_FULL \
    ATFULL(stack, 4)

#define PUSH(t) \
    __cporth_analyze_stack_push(stack, t, NULL, NULL)

#define PUSH_FULL(t, kw, generic) \
    __cporth_analyze_stack_push(stack, t, kw, generic)

#define PUSH_COPY(frame) \
    __cporth_analyze_stack_push_copy(stack, frame)

#define POP_N(n) \
    __cporth_analyze_stack_pop_n(stack, n)

#define POP() \
    POP_N(1)
#define POPPOP() \
    POP_N(2)
#define POP3() \
    POP_N(3)
#define POP4() \
    POP_N(4)
#define POP5() \
    POP_N(5)
#define POP6() \
    POP_N(6)
#define POP7() \
    POP_N(7)

#define POPPOPPUSH(t) \
    { POPPOP(); PUSH(t); }

#define POPPOPPUSHINT() \
    POPPOPPUSH(CPORTH_ANALYZE_DATA_INT)

#define POPPOPPUSHBOOL() \
    POPPOPPUSH(CPORTH_ANALYZE_DATA_BOOL)

#define POPPOPPUSHPTR() \
    POPPOPPUSH(CPORTH_ANALYZE_DATA_PTR)

#define POPPUSH(t) \
    { POP(); PUSH(t); }

#define POPPUSHINT() \
    POPPUSH(CPORTH_ANALYZE_DATA_INT)

#define POPPUSHBOOL() \
    POPPUSH(CPORTH_ANALYZE_DATA_BOOL)

#define POPPUSHPTR() \
    POPPUSH(CPORTH_ANALYZE_DATA_PTR)

#define PUSHGENERIC(generic) \
    PUSH_FULL(CPORTH_ANALYZE_DATA_GENERIC, NULL, generic)

#define PUSHINT() \
    PUSH(CPORTH_ANALYZE_DATA_INT)

#define PUSHBOOL() \
    PUSH(CPORTH_ANALYZE_DATA_BOOL)

#define PUSHPTR() \
    PUSH(CPORTH_ANALYZE_DATA_PTR)

#define DUP() \
    PUSH_COPY(TOP_FULL)

#define SWAP() \
    { DUP(); TOPTOP_FULL = TOP3_FULL; TOP3_FULL = TOP_FULL; POP(); }

#define OVER() \
    { PUSH_COPY(TOPTOP_FULL); }

#define ROT() \
    { PUSH_COPY(TOP3_FULL); TOP4_FULL = TOP3_FULL; TOP3_FULL = TOPTOP_FULL; TOPTOP_FULL = TOP_FULL; POP(); }

static inline int __cporth_analyze_stack_ensure_length (
		cporth_analyze_stack *stack,
		size_t count
) {
	if (stack->length >= count) {
		return 0;
	}
	return 1;
}

static inline int __cporth_analyze_stack_check_one_kw_struct (
		cporth_analyze_stack *stack
) {
	const struct cporth_keyword *kw = stack->frames[stack->length - 1].kw;
	return (
		kw != NULL && kw->id == CPORTH_KW_CP_STRUCT ? 0 : 1
	);
}

static inline int __cporth_analyze_stack_check_one (
		cporth_analyze_stack *stack,
		enum cporth_analyze_data a
) {
	return (
		stack->frames[stack->length - 1].data == a ? 0 : 1
	);
}

static inline int __cporth_analyze_stack_check_two (
		cporth_analyze_stack *stack,
		enum cporth_analyze_data a,
		enum cporth_analyze_data b
) {
	if ( TOP == a &&
	     TOPTOP == b
	) {
		return 0;
	}
	return 1;
}

static inline int __cporth_analyze_stack_equals (
		const cporth_analyze_stack *a,
		const cporth_analyze_stack *b
) {
	if (a->length != b->length) {
		return 0;
	}
	else if (a->length == 0) {
		return 1;
	}

	for (size_t n = 0; n < a->length; n++) {
		const cporth_analyze_frame *frame_a = a->frames + n;
		const cporth_analyze_frame *frame_b = b->frames + n;

		if (frame_a->data != frame_b->data) {
			return 0;
		}
		if (frame_a->data == CPORTH_ANALYZE_DATA_GENERIC && frame_a->generic == NULL) {
			cporth_err("A was null\n");
			return 0;
		}
		if (frame_a->data == CPORTH_ANALYZE_DATA_GENERIC &&
		    strcmp(frame_a->generic, frame_b->generic) != 0
		) {
			return 0;
		}
	}

	return 1;
}

static inline int __cporth_analyze_stack_top_equals (
		const cporth_analyze_stack *base,
		const cporth_analyze_stack *top
) {
	if (base->length < top->length)
		return 0;

	for (size_t n = 0; n < top->length; n++) {
		const cporth_analyze_data base_data = (base->frames + base->length - n - 1)->data;
		const cporth_analyze_data top_data = (top->frames + top->length - n - 1)->data;
		if (base_data != top_data && top_data != CPORTH_ANALYZE_DATA_GENERIC) {
			return 0;
		}
	}

	return 1;
}

#define ERR_CUSTOM_ARG(info,arg)                                                         \
    { cporth_err(info "\n", arg);                                                        \
    ret = CPORTH_ERR_HARD; break; } (void)(1)

#define ERR_CUSTOM(info)                                                                 \
    ERR_CUSTOM_ARG(info "%s\n", "")

#define ERR_OPERATOR_BASE(...)                  \
    { cporth_err(__VA_ARGS__);                  \
    ret = CPORTH_ERR_HARD; break; } (void)(1)

#define ERR_OPERATOR_TYPE(info,type)                                                            \
    ERR_OPERATOR_BASE("Incorrect data types on stack for operator %s (type is %s). " info "\n", \
        cporth_operator_names[t->op], type                                                      \
    )
    
#define ERR_OPERATOR(info)                                                               \
    ERR_OPERATOR_BASE("Incorrect data types on stack for operator %s. " info "\n",       \
        cporth_operator_names[t->op]                                                     \
    )

#define ERR_ENSURE(n) \
    {cporth_err("Not enough operands on the stack for operator %s, expected at least %u but encountered %lu.\n", cporth_operator_names[t->op], n, stack->length); ret = CPORTH_ERR_HARD; break; } (void)(1)

#define ERR_ENSURE_CONDITION() \
    {cporth_err("Missing operand on the stack for %s conditon.\n", t->kw->name); ret = CPORTH_ERR_HARD; break; } (void)(1)

#define HAVE_N(n) \
    (__cporth_analyze_stack_ensure_length(stack, n) == 0)

#define ENSURE_N(n) \
    if (!HAVE_N(n)) ERR_ENSURE(n)

#define ENSURE_ONE() \
    ENSURE_N(1)
#define ENSURE_TWO() \
    ENSURE_N(2)
#define ENSURE_THREE() \
    ENSURE_N(3)
#define ENSURE_FOUR() \
    ENSURE_N(4)
#define ENSURE_FIVE() \
    ENSURE_N(5)
#define ENSURE_SIX() \
    ENSURE_N(6)
#define ENSURE_SEVEN() \
    ENSURE_N(7)

#define ENSURE_ONE_CONDITION() \
    if (!HAVE_N(1)) ERR_ENSURE_CONDITION()

#define HAVE_KW_STRUCT \
    (__cporth_analyze_stack_check_one_kw_struct(stack) == 0)

#define HAVE_INT \
    (__cporth_analyze_stack_check_one(stack, CPORTH_ANALYZE_DATA_INT) == 0)

#define HAVE_PTR \
    (__cporth_analyze_stack_check_one(stack, CPORTH_ANALYZE_DATA_PTR) == 0)

#define HAVE_BOOL \
    (__cporth_analyze_stack_check_one(stack, CPORTH_ANALYZE_DATA_BOOL) == 0)

#define HAVE_INTINT \
    (__cporth_analyze_stack_check_two(stack, CPORTH_ANALYZE_DATA_INT, CPORTH_ANALYZE_DATA_INT) == 0)

#define HAVE_INTPTR \
    (__cporth_analyze_stack_check_two(stack, CPORTH_ANALYZE_DATA_INT, CPORTH_ANALYZE_DATA_PTR) == 0)

#define HAVE_PTRINT \
    (__cporth_analyze_stack_check_two(stack, CPORTH_ANALYZE_DATA_PTR, CPORTH_ANALYZE_DATA_INT) == 0)

#define HAVE_PTRPTR \
    (__cporth_analyze_stack_check_two(stack, CPORTH_ANALYZE_DATA_PTR, CPORTH_ANALYZE_DATA_PTR) == 0)

#define HAVE_BOOLBOOL \
    (__cporth_analyze_stack_check_two(stack, CPORTH_ANALYZE_DATA_BOOL, CPORTH_ANALYZE_DATA_BOOL) == 0)

static void __cporth_analyze_stack_push_from (
		cporth_analyze_stack *stack,
		const cporth_analyze_stack *source
) {
	for (size_t i = 0; i < source->length; i++) {
		PUSH_FULL((source->frames + i)->data, (source->frames + i)->kw, (source->frames + i)->generic);
	}
}

static void __cporth_analyze_stack_set (
		cporth_analyze_stack *stack,
		const cporth_analyze_stack *source
) {
	stack->length = 0;

	if (source != NULL) {
		__cporth_analyze_stack_push_from(stack, source);
	}
}

static int __cporth_analyze_stack_copy (
		cporth_analyze_stack **result,
		const cporth_analyze_stack *source,
		const cporth_program *program
) {
	int ret = CPORTH_OK;

	*result = NULL;

	cporth_analyze_stack *stack = NULL;

	if ((ret = __cporth_analyze_stack_new (&stack, program)) != 0) {
		goto out;
	}

	__cporth_analyze_stack_set (stack, source);

	*result = stack;

	out:
	return ret;
}

static void __cporth_analyze_stack_dump (
		cporth_analyze_stack *stack
) {
	char buf[512];

	sprintf(buf, "Stack types (top to bottom):");
	int max = 10;
	while (stack->length > 0 && max--) {
		if (TOP == CPORTH_ANALYZE_DATA_GENERIC) {
			sprintf(buf + strlen(buf), " \"%s\" ", TOP_FULL.generic);
		}
		else {
			sprintf(buf + strlen(buf), " %s ", cporth_analyze_data_names[TOP]);
		}
		POP();
	}
	sprintf(buf + strlen(buf), "%s\n", stack->length > 0 ? " ... " : "");

	cporth_err("%s", buf);
}

static void __cporth_analyze_stack_dump_const (
		const cporth_analyze_stack *stack,
		const cporth_token *t,
		const cporth_program *program
) {
	cporth_analyze_stack *stack_tmp = NULL;
	if (__cporth_analyze_stack_copy (
			&stack_tmp,
			stack,
			program
	) != 0) {
		assert(0);
	}

	cporth_program_err_custom_t(program, t, "Dumping stack");
	__cporth_analyze_stack_dump(stack_tmp);
}

static int __cporth_analyze_branch_verify (
		const cporth_program *program,
		cporth_analyze_stack *stack_true,
		cporth_analyze_stack *stack_false,
		const cporth_token *debug_t
) {
	if (!__cporth_analyze_stack_equals(stack_true, stack_false)) {
		cporth_program_err_custom_t(program, CPORTH_TOKEN_JUMPOF(debug_t), "Branch stack mismatch (true/false branch):");
		__cporth_analyze_stack_dump(stack_true);
		__cporth_analyze_stack_dump(stack_false);
		cporth_program_err_custom_t(program, debug_t, "In condition at");
		return CPORTH_ERR_HARD;
	}
	return CPORTH_OK;
}

static cporth_analyze_data __cporth_analyze_at_proc_declaration_generic_find (
		const cporth_analyze_stack *stack,
		const char *generic
) {
	for (size_t i = 0; i < stack->length; i++) {
		const cporth_analyze_frame *frame = stack->frames + i;
		if (frame->generic != NULL && strcmp(frame->generic, generic) == 0)
			return frame->data;
	}
	return CPORTH_ANALYZE_DATA_NONE;
}

static int __cporth_analyze_at_proc_declaration_generics_populate (
		cporth_analyze_stack *stack_input,
		const cporth_analyze_stack *stack_template

) {
	int ret = CPORTH_OK;

	assert(stack_template->length >= stack_input->length);

	for (size_t n = 0; n < stack_input->length; n++) {
		cporth_analyze_frame *frame_input = (stack_input->frames + stack_input->length - n - 1);
		const cporth_analyze_frame *frame_template = (stack_template->frames + stack_template->length - n - 1);

		if (frame_input->data != CPORTH_ANALYZE_DATA_GENERIC)
			continue;

		const cporth_analyze_data data_existing = __cporth_analyze_at_proc_declaration_generic_find(stack_input, frame_input->generic);
		if (data_existing != CPORTH_ANALYZE_DATA_GENERIC && data_existing != frame_template->data) {
			cporth_err("Different data types passed in generic type \"%s\" (%s and %s)\n",
				frame_input->generic, cporth_analyze_data_names[data_existing], cporth_analyze_data_names[frame_template->data]);
			ret = CPORTH_ERR_HARD;
			goto out;
		}

		frame_input->data = frame_template->data;
	}

	out:
	return ret;
}

static int __cporth_analyze_at_proc_declaration_generics_resolve (
		cporth_analyze_stack *stack_output,
		const cporth_analyze_stack *stack_input
) {
	int ret = CPORTH_OK;

	for (size_t n = 0; n < stack_output->length; n++) {
		cporth_analyze_frame *frame_output = (stack_output->frames + n);

		if (frame_output->data != CPORTH_ANALYZE_DATA_GENERIC)
			continue;

		const cporth_analyze_data data = __cporth_analyze_at_proc_declaration_generic_find(stack_input, frame_output->generic);
		if (data == CPORTH_ANALYZE_DATA_NONE) {
			// TODO : Make this assertion? check for this when parsing proc?
			cporth_err("Generic type \"%s\" not found in input parameters\n", frame_output->generic);
			ret = CPORTH_ERR_HARD;
			goto out;
		}

		frame_output->data = data;
	}

	out:
	return ret;
}

static void __cporth_analyze_at_get_proc_declaration (
		const cporth_token **last_ret,
		cporth_analyze_stack *stack_in,
		cporth_analyze_stack *stack_out,
		const cporth_token *begin,
		const cporth_token *end
) {
	const cporth_token *t = begin;
	cporth_analyze_stack *stack = stack_in;

	for (; t < end; t++) {
		*last_ret = t;

		switch (t->type) {
			case CPORTH_TP_STR:
			case CPORTH_TP_CSTR:
				PUSHGENERIC((const char *) t->ptr);
				break;
			case CPORTH_TP_KEYWORD:
				switch (t->kw->id) {
					case CPORTH_KW_INT:
						PUSHINT();
						break;
					case CPORTH_KW_PTR:
						PUSHPTR();
						break;
					case CPORTH_KW_BOOL:
						PUSHBOOL();
						break;
					case CPORTH_KW_BIKESHEDDER:
						assert(stack == stack_in);
						stack = stack_out;
						break;
					case CPORTH_KW_IN:
						return;
					default:
						assert(0);
				}
				break;
			default:
				assert(0);

		};

	}
}

static int __cporth_analyze_at (
		const cporth_token **last_ret,
		const cporth_program *program,
		const cporth_scan_ctx *scan_ctx,
		const cporth_token *begin,
		const cporth_token *end,
		cporth_analyze_stack *stack,
		const int recurse_level,
		size_t expected_stack_length_at_condition
) {
	int ret = CPORTH_OK;

	*last_ret = NULL;

	const cporth_token *t = begin;
	const cporth_token *last_tmp = NULL;

	cporth_analyze_stack *stack_a = NULL;
	cporth_analyze_stack *stack_b = NULL;

	for (; t < end && ret == CPORTH_OK; t++) {
		*last_ret = t;
	
		switch (t->type) {
			case CPORTH_TP_CALL:
				// Input arguments stack
				if ((ret = __cporth_analyze_stack_new (&stack_a, program)) != 0) {
					goto out;
				}

				// Output arguments stack
				if ((ret = __cporth_analyze_stack_new (&stack_b, program)) != 0) {
					goto out;
				}

				assert(CPORTH_TOKEN_JUMPOF(t)->kw->id == CPORTH_KW_PROC);

				__cporth_analyze_at_get_proc_declaration (
						&last_tmp,
						stack_a,
						stack_b,
						CPORTH_TOKEN_JUMPOF(t) + 2, /* Start after proc name */
						end
				);

				assert(last_tmp->kw->id == CPORTH_KW_IN);

				if (stack->length < stack_a->length) {
					cporth_err("Not enough operands on the stack for proc call (expected/actual)\n");
					__cporth_analyze_stack_dump(stack_a);
					__cporth_analyze_stack_dump(stack);
					cporth_program_err_custom_t(program, t, "In proc call at");
					ret = CPORTH_ERR_HARD;
					goto out;
				}

				if ((ret = __cporth_analyze_at_proc_declaration_generics_populate (stack_a, stack)) != 0) {
					cporth_program_err_custom_t(program, t, "In proc call at");
					goto out;
				}

				if (!__cporth_analyze_stack_top_equals(stack, stack_a)) {
					cporth_program_err_custom_t(program, CPORTH_TOKEN_JUMPOF(t), "Incorrect types on stack for proc call (expected/actual):");
					__cporth_analyze_stack_dump(stack_a);
					__cporth_analyze_stack_dump(stack);
					cporth_program_err_custom_t(program, t, "In proc call at");
					ret = CPORTH_ERR_HARD;
					goto out;
				}

				if ((ret = __cporth_analyze_at_proc_declaration_generics_resolve (stack_b, stack_a)) != 0) {
					cporth_program_err_custom_t(program, t, "In proc call at");
					goto out;
				}

				stack->length -= stack_a->length;
				__cporth_analyze_stack_push_from (stack, stack_b);

				__cporth_analyze_stack_destroy(stack_a);
				__cporth_analyze_stack_destroy(stack_b);

				stack_a = NULL;
				stack_b = NULL;

				// Continue after call

				break;
			case CPORTH_TP_WORD:
				if (t->kw != NULL)
					break;
				switch (t->op) {
					case CPORTH_OP_PLUS:
						ENSURE_TWO();
						if (HAVE_INTINT)
							POPPOPPUSHINT()
						else if (HAVE_INTPTR || HAVE_PTRINT)
							POPPOPPUSHPTR()
						else
							ERR_OPERATOR("Expected INT or PTR.");
						break;
					case CPORTH_OP_MINUS:
						ENSURE_TWO();
						if (HAVE_INTINT || HAVE_PTRPTR)
							POPPOPPUSHINT()
						else if (HAVE_INTPTR)
							POPPOPPUSHPTR()
						else
							ERR_OPERATOR("Cannot subtract PTR from INT.");
						break;
					case CPORTH_OP_DIVMOD:
						ENSURE_TWO();
						if (!HAVE_INTINT)
							ERR_OPERATOR("Expected INT.");
						break;
					case CPORTH_OP_MUL:
					case CPORTH_OP_SHR:
					case CPORTH_OP_SHL:
					case CPORTH_OP_MAX:
						ENSURE_TWO();
						if (HAVE_INTINT)
							POPPOPPUSHINT()
						else
							ERR_OPERATOR("Expected INT.");
						break;
					case CPORTH_OP_EQ:
					case CPORTH_OP_GT:
					case CPORTH_OP_LT:
					case CPORTH_OP_GE:
					case CPORTH_OP_LE:
					case CPORTH_OP_NE:
						ENSURE_TWO();
						if (HAVE_INTINT || HAVE_PTRPTR)
							POPPOPPUSHBOOL()
						else
							ERR_OPERATOR("Expected INT or PTR.");
						break;
					case CPORTH_OP_OR:
					case CPORTH_OP_AND:
						ENSURE_TWO();
						if (HAVE_INTINT)
							POPPOPPUSHINT()
						else if (HAVE_BOOLBOOL)
							POPPOPPUSHBOOL()
						else
							ERR_OPERATOR("Expected INT or BOOL.");
						break;
					case CPORTH_OP_NOT:
						ENSURE_ONE();
						if (!(HAVE_INT || HAVE_BOOL))
							ERR_OPERATOR("Expected INT or BOOL.");
						break;
					case CPORTH_OP_PRINT:
						ENSURE_ONE();
						POP();
						break;
					case CPORTH_OP_DUP:
						ENSURE_ONE();
						DUP();
						break;
					case CPORTH_OP_SWAP:
						ENSURE_TWO();
						SWAP();
						break;
					case CPORTH_OP_DROP:
						ENSURE_ONE();
						POP();
						break;
					case CPORTH_OP_OVER:
						ENSURE_TWO();
						OVER();
						break;
					case CPORTH_OP_ROT:
						ENSURE_THREE();
						ROT();
						break;
					case CPORTH_OP_LOAD8:
					case CPORTH_OP_LOAD16:
					case CPORTH_OP_LOAD32:
					case CPORTH_OP_LOAD64:
						ENSURE_ONE();
						if (HAVE_PTR) {
							if (HAVE_KW_STRUCT) {
								ERR_OPERATOR_TYPE("Refusing to load from pointer to struct, must cast first.", TOP_FULL.kw->name);
							}
							else {
								POPPUSHINT()
							}
						}
						else {
							ERR_OPERATOR("Expected PTR.");
						}
						break;
					case CPORTH_OP_STORE8:
					case CPORTH_OP_STORE16:
					case CPORTH_OP_STORE32:
						ENSURE_TWO();
						if (HAVE_PTR) {
							if (HAVE_KW_STRUCT) {
								ERR_OPERATOR_TYPE("Refusing to store to pointer to struct, must cast first.", TOP_FULL.kw->name);
							}
							else {
								POPPOP();
							}
						}
						else {
							ERR_OPERATOR("Expected PTR and some other operand.");
						}
						break;
					case CPORTH_OP_STORE64:
						ENSURE_TWO();
						if (HAVE_PTR)
							POPPOP();
						else
							ERR_OPERATOR("Expected PTR and some other operand.");
						break;
					case CPORTH_OP_CAST_PTR:
						ENSURE_ONE();
						POPPUSHPTR();
						break;
					case CPORTH_OP_CAST_INT:
						ENSURE_ONE();
						POPPUSHINT();
						break;
					case CPORTH_OP_CAST_BOOL:
						ENSURE_ONE();
						POPPUSHBOOL();
						break;
					case CPORTH_OP_ARGC:
						PUSHINT();
						break;
					case CPORTH_OP_ARGV:
					case CPORTH_OP_ENVP:
						PUSHPTR();
						break;
					case CPORTH_OP_SYSCALL0:
						ENSURE_ONE();
						POPPUSHINT();
						break;
					case CPORTH_OP_SYSCALL1:
						ENSURE_TWO();
						POP();
						POPPUSHINT();
						break;
					case CPORTH_OP_SYSCALL2:
						ENSURE_THREE();
						POPPOP();
						POPPUSHINT();
						break;
					case CPORTH_OP_SYSCALL3:
						ENSURE_FOUR();
						POP3();
						POPPUSHINT();
						break;
					case CPORTH_OP_SYSCALL4:
						ENSURE_FIVE();
						POP4();
						POPPUSHINT();
						break;
					case CPORTH_OP_SYSCALL5:
						ENSURE_SIX();
						POP5();
						POPPUSHINT();
						break;
					case CPORTH_OP_SYSCALL6:
						ENSURE_SEVEN();
						POP6();
						POPPUSHINT();
						break;
					case CPORTH_OP_OFFSET:
						if (scan_ctx == NULL) {
							ERR_OPERATOR("Not allowed outside constant context");
						}
						ENSURE_ONE();
						POPPUSHINT();
						break;
					case CPORTH_OP_RESET:
						PUSHINT();
						if (scan_ctx == NULL) {
							ERR_OPERATOR("Not allowed outside constant context");
						}
						break;
					case CPORTH_OP_DBG_STACKDUMP:
						__cporth_analyze_stack_dump_const(stack, t, program);
						break;
					case CPORTH_OP_NUMBER_OF:
						assert(0);
				};
				break;
			case CPORTH_TP_INT:
				PUSH(CPORTH_ANALYZE_DATA_INT);
				break;
			case CPORTH_TP_BOOL:
				PUSH(CPORTH_ANALYZE_DATA_BOOL);
				break;
			case CPORTH_TP_PTR:
				PUSH_FULL(CPORTH_ANALYZE_DATA_PTR, t->kw, NULL);
				break;
			case CPORTH_TP_STR:
				PUSH(CPORTH_ANALYZE_DATA_INT);
				PUSH(CPORTH_ANALYZE_DATA_PTR);
				break;
			case CPORTH_TP_CSTR:
				PUSH(CPORTH_ANALYZE_DATA_PTR);
				break;
			case CPORTH_TP_CHAR:
				PUSH(CPORTH_ANALYZE_DATA_INT);
				break;
			case CPORTH_TP_KEYWORD:
				switch (t->kw->id) {
					case CPORTH_KW_IFSTAR:
					case CPORTH_KW_DO:
						assert (expected_stack_length_at_condition != SIZE_MAX);

						if (stack->length != expected_stack_length_at_condition) {
							cporth_program_err_custom_t(program, t, "Not excactly one extra operand at condition");
							ret = CPORTH_ERR_HARD;
							break;
						}
						expected_stack_length_at_condition = SIZE_MAX;

						/* Fallthrough */
					case CPORTH_KW_IF:
						ENSURE_ONE_CONDITION();
						if (!HAVE_BOOL)
							ERR_CUSTOM("Expected BOOL at if condition");
						POP();

						if (t->kw->id == CPORTH_KW_IFSTAR) {
							break;
						}

						expected_stack_length_at_condition = stack->length + 1;

						assert (stack_a == NULL && stack_b == NULL && t->jump.i != 0);

						// True branch stack
						if ((ret = __cporth_analyze_stack_copy (&stack_a, stack, program)) != 0) {
							goto out;
						}

						// False branch stack
						if ((ret = __cporth_analyze_stack_new (&stack_b, program)) != 0) {
							goto out;
						}

						const cporth_token *jump = CPORTH_TOKEN_JUMPOF(t);

						if ((ret = __cporth_analyze_at (
								&last_tmp,
								program,
								scan_ctx,
								t + 1,
								end,
								stack_a,
								recurse_level + 1,
								expected_stack_length_at_condition
						)) != CPORTH_END && ret != CPORTH_EOF) {
							assert(ret != CPORTH_OK);
							cporth_program_err_custom_t(program, t, "In condition at");
							break;
						}

						if (last_tmp->kw->id == CPORTH_KW_END) {
							if ((ret = __cporth_analyze_branch_verify (program, stack_a, stack, t)) != 0) {
								goto err;
							}
						}
						else {
							while (last_tmp->kw->id == CPORTH_KW_SP_ORELSE || last_tmp->kw->id == CPORTH_KW_ELSE) {
								__cporth_analyze_stack_set (stack_b, stack);

								if ((ret = __cporth_analyze_at (
										&last_tmp,
										program,
										scan_ctx,
										jump + 1,
										end,
										stack_b,
										recurse_level + 1,
										expected_stack_length_at_condition
								)) != CPORTH_END && ret != CPORTH_EOF) {
									assert(ret != CPORTH_OK);
									cporth_program_err_custom_t(program, CPORTH_TOKEN_JUMPOF(t), "In false branch at");
									cporth_program_err_custom_t(program, t, "In condition at");
									goto err;
								}

								if ((ret = __cporth_analyze_branch_verify (program, stack_a, stack_b, t)) != 0) {
									goto err;
								}

								jump = last_tmp;
								stack_b->length = 0;
							}
						}

						assert (last_tmp->kw->id == CPORTH_KW_END);

						__cporth_analyze_stack_set (stack, stack_a);

						__cporth_analyze_stack_destroy(stack_a);
						__cporth_analyze_stack_destroy(stack_b);

						stack_a = NULL;
						stack_b = NULL;

						t = last_tmp;

						break;
					case CPORTH_KW_MACRO:
					case CPORTH_KW_CONST:
					case CPORTH_KW_MEMORY:
					case CPORTH_KW_ASSERT:
					case CPORTH_KW_HERE:
						t = CPORTH_TOKEN_JUMPOF_OR_SELF(t);
						break;
					case CPORTH_KW_PROC:
						// Input arguments stack
						if ((ret = __cporth_analyze_stack_new (&stack_a, program)) != 0) {
							goto out;
						}

						// Output arguments stack
						if ((ret = __cporth_analyze_stack_new (&stack_b, program)) != 0) {
							goto out;
						}

						__cporth_analyze_at_get_proc_declaration (
								&last_tmp,
								stack_a,
								stack_b,
								t + 2,
								end
						);

						jump = CPORTH_TOKEN_JUMPOF_OR_SELF(t);
						assert(jump->kw->id == CPORTH_KW_IN);
						assert(last_tmp == jump);

						if ((ret = __cporth_analyze_at (
								&last_tmp,
								program,
								scan_ctx,
								jump + 1,
								end,
								stack_a, /* Pass input arguments */
								recurse_level + 1,
								0
						)) != CPORTH_END && ret != CPORTH_EOF) {
							assert(ret != CPORTH_OK);
							cporth_program_err_custom_t(program, t, "In proc declaration at");
							goto err;
						}

						ret = CPORTH_OK;

						jump = CPORTH_TOKEN_JUMPOF_OR_SELF(jump);
						assert(jump->kw->id == CPORTH_KW_END);
						assert(last_tmp == jump);

						if (!__cporth_analyze_stack_equals(stack_a, stack_b)) {
							cporth_program_err_custom_t(program, jump, "Incorrect types on stack after proc (expected/actual):");
							__cporth_analyze_stack_dump(stack_b);
							__cporth_analyze_stack_dump(stack_a);
							cporth_program_err_custom_t(program,t, "In proc declaration at");
							ret = CPORTH_ERR_HARD;
							goto out;
						}

						__cporth_analyze_stack_destroy(stack_a);
						__cporth_analyze_stack_destroy(stack_b);

						stack_a = NULL;
						stack_b = NULL;

						t = last_tmp;

						break;
					case CPORTH_KW_CP_STRUCT:
						// Jump over struct definition or past instantiation name
						t = (CPORTH_TOKEN_JUMPOF(t) != NULL ? CPORTH_TOKEN_JUMPOF(t) : t + 1);
						break;
					case CPORTH_KW_WHILE:
						expected_stack_length_at_condition = stack->length + 1;
						break;
					case CPORTH_KW_SP_ORELSE:
					case CPORTH_KW_END:
					case CPORTH_KW_ELSE:
						ret = CPORTH_END;
						goto out;
					case CPORTH_KW_INCLUDE:
						t++;
						continue;
					case CPORTH_KW_CP_VAR:
					case CPORTH_KW_INT:
					case CPORTH_KW_PTR:
					case CPORTH_KW_BOOL:
					case CPORTH_KW_BIKESHEDDER:
					case CPORTH_KW_IN:
						assert(0);
				};
				break;
		};

		err:

		if (ret != CPORTH_OK) {
			__cporth_analyze_stack_dump(stack);
			cporth_program_err_descend_t(program, t >= end ? end - 1 : t);
			break;
		}
	}

	if (ret == 0 && t == program->tokens + program->tokens_length) {
		ret = CPORTH_EOF;
	}

	out:
	if (stack_a != NULL) {
		__cporth_analyze_stack_destroy(stack_a);
	}
	if (stack_b != NULL) {
		__cporth_analyze_stack_destroy(stack_b);
	}
	return ret;
}

static int __cporth_analyze_start (
		size_t *stack_length,
		const cporth_program *program,
		const cporth_scan_ctx *scan_ctx,
		const cporth_token *begin,
		const cporth_token *end,
		const int recurse_level,
		int (*stack_result_callback)(cporth_analyze_data data, void *arg),
		void *stack_result_callback_arg
) {
	int ret = CPORTH_OK;

	*stack_length = 0;

	cporth_analyze_stack *stack = NULL;

	if ((ret = __cporth_analyze_stack_new (&stack, program)) != 0) {
		goto out;
	}

	const cporth_token *last_dummy;
	if ((ret = __cporth_analyze_at (
			&last_dummy,
			program,
			scan_ctx,
			begin,
			end,
			stack,
			recurse_level,
			SIZE_MAX
	)) != 0) {
		goto out_free;
	}

	if (stack_result_callback != NULL) {
		if (stack->length != 1) {
			cporth_err("Expected excactly one operand on the stack but %lu were encountered\n", stack->length);
			ret = CPORTH_ERR_HARD;
			goto out_free;
		}
		if ((ret = stack_result_callback((stack->frames + stack->length - 1)->data, stack_result_callback_arg)) != 0) {
			goto out_free;
		}
	}

	out_free:
		*stack_length = stack->length;
		__cporth_analyze_stack_destroy(stack);
	out:
		return ret;
}

int cporth_analyze_partial (
		size_t *stack_length,
		const cporth_program *program,
		const cporth_scan_ctx *scan_ctx,
		const cporth_token *begin,
		const cporth_token *end,
		int (*callback)(cporth_analyze_data data, void *arg),
		void *arg
) {
	return __cporth_analyze_start (
			stack_length,
			program,
			scan_ctx,
			begin,
			end,
			0,
			callback,
			arg
	);
}

int cporth_analyze (
		const cporth_program *program
) {
	int ret = CPORTH_OK;

	size_t stack_length;
	if ((ret = __cporth_analyze_start (
			&stack_length,
			program,
			NULL,
			program->tokens,
			program->tokens + program->tokens_length,
			0,
			NULL,
			NULL
	)) != 0 && ret != CPORTH_EOF) {
		goto out;
	}

	if (stack_length != 0) {
		cporth_err("%lu operands remaining on stack on program end\n", stack_length);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	out:
	return ret;
}
