#include "program.h"
#include "scan.h"
#include "parse.h"
#include "location.h"

int cporth_main_file_to_program (
		const char *filename,
		int (*callback)(const cporth_program *program, void *arg),
		void *arg
) {
	int ret = 0;

	cporth_program *program_tmp = NULL;

	// Step 1: Scanning, tokenizing, includes and macro expansion

	if ((ret = cporth_scan_init()) != 0) {
		goto out;
	}

	if ((ret = cporth_program_new (&program_tmp)) != 0) {
		goto out;
	}

	if ((ret = cporth_scan_file_to_program(program_tmp, filename)) != 0) {
		goto out;
	}

	// Step 2: Parsing / intrinsic name resolving

	if ((ret = cporth_parse(program_tmp)) != 0) {
		goto out;
	}

	// Step 3: Analyze, run/compile performed as needed by callback

	if ((ret = callback(program_tmp, arg)) != 0) {
		goto out;
	}

	out:
	if (program_tmp != NULL) {
		cporth_program_destroy(program_tmp);
	}

	cporth_scan_cleanup();

	return ret;
}
