#ifndef CPORTH_PROGRAM_STRUCT_H
#define CPORTH_PROGRAM_STRUCT_H

#include <stddef.h>

#include "token.h"
#include "location.h"

typedef struct cporth_token cporth_token;
typedef struct cporth_location cporth_location;

typedef struct cporth_program {
	cporth_token *tokens;
	cporth_location *locations;
	size_t tokens_size;
	size_t tokens_length;
} cporth_program;

static inline cporth_token_ref __cporth_program_token_to_tref (
		const cporth_program *program,
		const cporth_token *token
) {
	cporth_token_ref tref = {
		((size_t) token - (size_t) program->tokens) / sizeof(*token)
	};
	return tref;
}

static inline cporth_location_ref __cporth_program_tref_to_lref (
		const cporth_token_ref tref
) {
	const cporth_location_ref lref = { tref.i };
	return lref;
}

#define CPORTH_PROGRAM_REF_TO_LOCATION(ref) \
    (program->locations + ref.i)

#define CPORTH_PROGRAM_REF_TO_TOKEN(ref) \
    (program->tokens + ref.i)

#define CPORTH_PROGRAM_TOKEN_TO_LREF(token) \
    (__cporth_program_tref_to_lref(__cporth_program_token_to_tref(program, token)))

#define CPORTH_PROGRAM_TOKEN_TO_TREF(token) \
    (__cporth_program_token_to_tref(program, token))

#define CPORTH_PROGRAM_LOCATION_PARENT_SET_FROM_REF(loc, locr) \
    (loc).parent_ref.i = locr.i

#define CPORTH_PROGRAM_TOKEN_JUMP_SET_FROM_REF(token, ref) \
    (token)->jump.i = (ref.i) - (((size_t) (token) - (size_t) program->tokens) / sizeof(*token))

static inline cporth_token *__cporth_program_token_top (
		cporth_program *program
) {
	assert(program->tokens_length >= 1);
	return program->tokens + program->tokens_length - 1;
}

static inline cporth_token_ref __cporth_program_token_top_ref (
		cporth_program *program
) {
	assert(program->tokens_length >= 1);
	cporth_token_ref ret = {program->tokens_length - 1};
	return ret;
}

static inline void *__cporth_program_token_top_ptr (
		cporth_program *program
) {
	assert(program->tokens_length >= 1);
	return (program->tokens + program->tokens_length - 1)->ptr;
}

static inline cporth_location *__cporth_program_location_top (
		cporth_program *program
) {
	return program->locations + program->tokens_length - 1;
}

static inline cporth_location_ref __cporth_program_location_top_ref (
		cporth_program *program
) {
	assert(program->tokens_length >= 1);
	cporth_location_ref ret = {program->tokens_length - 1};
	return ret;
}

static inline cporth_location_ref __cporth_program_location_toptop_ref (
		cporth_program *program
) {
	assert(program->tokens_length >= 2);
	cporth_location_ref ret = {program->tokens_length - 2};
	return ret;
}

static inline void __cporth_program_token_pop (
		cporth_program *program
) {
	assert(program->tokens_length >= 1);
	program->tokens_length--;
	cporth_token_cleanup(program->tokens + program->tokens_length);
}

#define CPORTH_PROGRAM_TOKEN_TOP \
    __cporth_program_token_top(program)

#define CPORTH_PROGRAM_TOKEN_TOP_REF \
    __cporth_program_token_top_ref(program)

#define CPORTH_PROGRAM_TOKEN_TOP_PTR \
    __cporth_program_token_top_ptr(program)

#define CPORTH_PROGRAM_LOCATION_TOP \
    __cporth_program_location_top(program)

#define CPORTH_PROGRAM_LOCATION_TOP_REF \
    __cporth_program_location_top_ref(program)

#define CPORTH_PROGRAM_LOCATION_TOPTOP_REF \
    __cporth_program_location_toptop_ref(program)

#define CPORTH_PROGRAM_TOKEN_POP() \
   __cporth_program_token_pop(program)

#endif /* CPORTH_PROGRAM_STRUCT_H */
