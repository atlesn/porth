#ifndef CPORTH_UTIL_H
#define CPORTH_UTIL_H

#include "allocate.h"

#define CPORTH_FREE_IF_NOT_NULL(ptr) \
	if (ptr != NULL) cporth_free(ptr)

#endif /* CPORTH_UTIL_H */
