#ifndef CPORTH_FILE_H
#define CPORTH_FILE_H

#include <stddef.h>

#include "location.h"

typedef struct cporth_file cporth_file;

int cporth_file_new (
		cporth_file **file,
		const char *filename
);
void cporth_file_destroy (
		cporth_file *file
);
void cporth_file_location_buf_set (
		cporth_location *loc,
		const cporth_file *file
);

#endif /* CPORTH_FILE_H */
