#ifndef CPORTH_ERR_H
#define CPORTH_ERR_H

#define CPORTH_OK         0
#define CPORTH_ERR_HARD   1
#define CPORTH_EOF        2
#define CPORTH_NOT_FOUND  CPORTH_EOF
#define CPORTH_END        4
#define CPORTH_DONT       8
#define CPORTH_DO         16

#endif /* CPORTH_ERR_H */
