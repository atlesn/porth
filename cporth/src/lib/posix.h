#ifndef CPORTH_POSIX_H
#define CPORTH_POSIX_H

#include <string.h>

static const char *cporth_strerror(int err) {
	// When needed, add locking here
	return strerror(err);
}

#undef strerror
#define strerror(...) __intercepted_illegal_strerror()

#endif /* CPORTH_POSIX_H */
