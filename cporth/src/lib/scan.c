#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <inttypes.h>

#include "scan.h"
#include "scan_ctx.h"
#include "program.h"
#include "program_struct.h"
#include "file.h"
#include "log.h"
#include "error.h"
#include "dict.h"
#include "location.h"
#include "token.h"
#include "util.h"
#include "parse.h"
#include "analyze.h"
#include "run.h"
#include "operator.h"

#define INCLUDE_NESTING_MAX 100

static void __cporth_scan_ignore_spaces (
		cporth_location *loc
) {
	for (; loc->pos < loc->length; loc->pos++) {
		switch (loc->buf[loc->pos]) {
			case '\n':
				loc->col_pos = 1;
				loc->row_pos++;
				break;
			case '\r':
			case ' ':
			case '\t':
				loc->col_pos++;
				break;
			default:
				return;
		};
	}
}

static void __cporth_scan_to_newline (
		cporth_location *loc
) {
	for (; loc->pos < loc->length; loc->pos++) {
		if (loc->buf[loc->pos] == '\n')
			return;
		loc->col_pos++;
	}
}

static int __cporth_scan_to_space (
		cporth_location *loc
) {
	int ret = CPORTH_OK;

	for (; loc->pos < loc->length; loc->pos++) {
		switch (loc->buf[loc->pos]) {
			case '\r':
			case '\n':
			case ' ':
			case '\t':
				goto out;
			case '\0':
				cporth_err("Unexpected null byte\n");
				ret = CPORTH_ERR_HARD;
				goto out;
			default:
				break;
		};
		loc->col_pos++;
	}

	out:
	return ret;
}

static int __cporth_scan_check_char (
		cporth_location *loc,
		const char c
) {
	if (loc->pos < loc->length) {
		if (loc->buf[loc->pos] == c) {
			loc->col_pos++;
			loc->pos++;
			return 1;
		}
	}
	return 0;
}

struct cporth_scan_callback_data {
	cporth_program *program;
	cporth_scan_ctx *scan_ctx;
	cporth_location *loc;
	cporth_token_ref nest_begin;
	int nest_level;
	const void *memory;
	size_t memory_size;
	size_t memory_pos;
};

#define POSTPROCESS_CALLBACK_DATA_SET()                                    \
    struct cporth_scan_callback_data *callback_data = arg;                 \
    (void)(kw);                                                            \
    cporth_program *program = callback_data->program; (void)(program);     \
    cporth_scan_ctx *scan_ctx = callback_data->scan_ctx; (void)(scan_ctx); \
    cporth_location *loc = callback_data->loc; (void)(loc)

#define SCAN_CALLBACK_DATA_SET()                                           \
    POSTPROCESS_CALLBACK_DATA_SET()

#define CHECK_END()                 \
    if (loc->pos >= loc->length)    \
        return CPORTH_EOF

static void __cporth_scan_token_resolve (
		cporth_token *token,
		const cporth_keyword *kw
) {
	assert(token->type == CPORTH_TP_WORD);
	token->kw = kw;
	token->type = CPORTH_TP_KEYWORD;
}

static int __cporth_scan_word_split_resolve (
		const cporth_keyword **kw,
		const void **memory,
		size_t *memory_size,
		size_t *memory_pos,
		const cporth_dict * const dict,
		const char *name
) {
	int ret = CPORTH_OK;

	*memory = NULL;
	*memory_size = 0;
	*memory_pos = 0;
	*kw = NULL;

	const cporth_keyword *kw_base = NULL;
	const size_t name_len = strlen(name);
	char *buf;
	const void *memory_tmp = NULL;
	size_t memory_pos_tmp = 0;
	size_t memory_size_tmp = 0;

	if ((buf = cporth_allocate(name_len + 1)) == NULL) {
		cporth_err("Could not allocate memory in %s\n", __func__);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	memcpy(buf, name, name_len + 1);

	const cporth_dict *cur_dict = dict;
	const char *end = buf + name_len;
	const char *last = end - 1;
	char *start = buf;
	char *pos = buf;

	if (*last == '.') {
		cporth_err("Missing name after '.' in '%s'\n", start);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	for (; pos <= last; pos++) {
		if (*pos == '.' || pos == last) {
			if (pos != last) {
				*pos = '\0';
				if (kw_base != NULL) {
					// More dots, switch dictionary
					cur_dict = &kw_base->dict;
				}
			}
			if (kw_base == NULL) {
				const cporth_dict_entry *dict_entry = NULL;

				// Look for struct
				if ((dict_entry = cporth_dict_lookup (cur_dict, start)) == NULL) {
					goto out;
				}
				// OK if last position and not struct, also when there are no dots
				if (pos != last && dict_entry->kw->id != CPORTH_KW_CP_STRUCT) {
					// TODO : Enable this check later. For now, allow full names with . in them.
					// cporth_err("Keyword %s was not a struct while accessing member using '.'.\n", dict_entry->kw->name);
					// ret = CPORTH_ERR_HARD;
					goto out;
				}
				memory_tmp = dict_entry->kw->memory;
				memory_size_tmp = dict_entry->memory_size;
				kw_base = dict_entry->kw;
				start = pos + 1;
			}
			else {
				const cporth_dict_entry *dict_entry = NULL;

				if ((dict_entry = cporth_dict_lookup_no_parent (&kw_base->dict, start)) == NULL &&
				    (dict_entry = cporth_dict_lookup_no_parent (kw_base->dict.parent, start)) == NULL
				) {
					goto out;
				}

				if (dict_entry->kw->data != CPORTH_ANALYZE_DATA_NONE) {
					//printf("- Found private member pos %lu\n", dict_entry->memory_pos);
					memory_pos_tmp += dict_entry->memory_pos;
					assert(memory_tmp == NULL || dict_entry->memory_size + memory_pos_tmp <= memory_size_tmp);
				}
				else {
					// printf("- Found static member memory size %lu\n", dict_entry->memory_size);
					memory_tmp = dict_entry->kw->memory;
					memory_size_tmp = dict_entry->memory_size;
					memory_pos_tmp = 0;
				}

				kw_base = dict_entry->kw;
				start = pos + 1;
			}
		}
	}

	assert (kw_base != NULL);

	*kw = kw_base;
	*memory = memory_tmp;
	*memory_size = memory_size_tmp;
	*memory_pos = memory_pos_tmp;

	out:
	CPORTH_FREE_IF_NOT_NULL(buf);
	return ret;
}


static int __cporth_scan_word_postprocess (CPORTH_PROGRAM_KW_POSTPROCESS_ARGS) {
	POSTPROCESS_CALLBACK_DATA_SET();

	int ret = CPORTH_OK;

	if (token->type == CPORTH_TP_INT) {
		goto out;
	}

	// Check for struct member (dot separated) or whole word
	if (kw == NULL && (ret = __cporth_scan_word_split_resolve (
			&kw,
			&callback_data->memory,
			&callback_data->memory_size,
			&callback_data->memory_pos,
			scan_ctx->dict,
			(const char *) token->ptr
	)) != 0) {
		goto out;
	}

	// Check for complete name match (backwards compatibility)
	if (kw == NULL) {
		kw = cporth_dict_kw_lookup(scan_ctx->dict, (const char *) token->ptr);
	}

	//printf("Scan result of %s memory %p size %lu pos %lu\n", token->ptr, callback_data->memory, callback_data->memory_size, callback_data->memory_pos);

	if (kw != NULL) {
		__cporth_scan_token_resolve(token, kw);

		if (kw->scan != NULL && (ret = kw->scan(kw, arg)) != 0) {
			goto out;
		}
	}

	out:
	return ret;
}

static int __cporth_scan_word_final (
		cporth_program *program,
		cporth_location *loc,
		const char *word,
		size_t length,
		const cporth_location *loc_orig,
		int (*postprocess)(CPORTH_PROGRAM_KW_POSTPROCESS_ARGS),
		const cporth_keyword *kw,
		struct cporth_scan_callback_data *callback_data
) {
	int ret = CPORTH_OK;

	union {
		int64_t s;
		uint64_t u;
	} num;

	if (length < 32) {
		char buf[32];
		memcpy(buf, word, length);
		buf[length] = '\0';
		// TODO : Support hex 0x, octal 0111 etc. by setting base to 0
		char *endptr = NULL;
		errno = 0;
		num.s = strtoll(buf, &endptr, 10);
		if (errno != ERANGE && *endptr == '\0') {
			goto out_push_int;
		}

		endptr = NULL;
		errno = 0;
		num.u = strtoull(buf, &endptr, 10);
		if (errno != ERANGE && *endptr == '\0') {
			goto out_push_int;
		}
	}

	if ((ret = cporth_program_token_push (
			program,
			CPORTH_TP_WORD,
			word,
			length,
			*loc_orig,
			postprocess,
			kw,
			callback_data
	)) != 0) {
		goto out;
	}

	out:
	return ret;

	out_push_int:
	return cporth_program_token_push (
			program,
			CPORTH_TP_INT,
			&num,
			sizeof(num),
			*loc,
			postprocess,
			kw,
			callback_data
	);
}

static int __cporth_scan_word (
		cporth_program *program,
		cporth_location *loc,
		const cporth_location *loc_orig,
		int (*postprocess)(CPORTH_PROGRAM_KW_POSTPROCESS_ARGS),
		const cporth_keyword *kw,
		struct cporth_scan_callback_data *callback_data
) {
	return __cporth_scan_word_final (
			program,
			loc,
			loc->buf + loc_orig->pos,
			loc->pos - loc_orig->pos,
			loc_orig,
			postprocess,
			kw,
			callback_data
	);
}

static int __cporth_scan_quoted_string (
		cporth_program *program,
		cporth_location *loc,
		const char end,
		cporth_token_type type
) {
	int ret = CPORTH_OK;

	char *buf = NULL;

	size_t final_length = 0;
	size_t quoted_length = 0;

	int is_terminated = 0;
	int is_escape = 0;

	const cporth_location loc_orig = *loc;

	for (size_t pos = loc->pos; pos < loc->length; pos++) {
		quoted_length++;

		const char c = *(loc->buf + pos);

		if (is_escape) {
			is_escape = 0;
			final_length++;
		}
		else if (c == '\\') {
			is_escape = 1;
		}
		else if (c == end) {
			is_terminated = 1;
			break;
		}
		else {
			final_length++;
		}
	}

	if (!is_terminated) {
		cporth_err("Unterminated quoted string\n");
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	if ((buf = cporth_allocate(final_length + 1)) == NULL) {
		cporth_err("Failed to allocate %lu bytes for quoted strinng\n", final_length + 1);
	}

	size_t wpos = 0;
	while (wpos < final_length) {
		const char c = *(loc->buf + loc->pos);

		if (is_escape) {
			is_escape = 0;
			switch (c) {
				case 'r':
					buf[wpos] = '\r';
					break;
				case 'n':
					buf[wpos] = '\n';
					break;
				case 't':
					buf[wpos] = '\t';
					break;
				case 'b':
					buf[wpos] = '\b';
					break;
				case '0':
					buf[wpos] = '\0';
					break;
				case '\'':
					buf[wpos] = '\\';
					break;
				case '"':
					buf[wpos] = '"';
					break;
				default:
					buf[wpos] = c;
					break;
			};
			wpos++;
		}
		else if (c == '\\') {
			is_escape = 1;
		}
		else if (c == end) {
			break;
		}
		else {
			buf[wpos] = c;
			wpos++;
		}

		loc->pos++;
		if (c == '\n') {
			loc->col_pos = 1;
			loc->row_pos++;
		}
		else {
			loc->col_pos++;
		}
	}
	buf[wpos] = '\0';

	assert(*((char *) loc->buf + loc->pos) == end);
	loc->pos++;
	loc->col_pos++;

	if ((ret = cporth_program_token_push (
			program,
			type,
			buf,
			final_length,
			loc_orig,
			NULL,
			NULL,
			NULL
	)) != 0) {
		goto out;
	}

	out:
	CPORTH_FREE_IF_NOT_NULL(buf);
	return ret;
}

static int __cporth_scan_str (
		cporth_program *program,
		cporth_location *loc
) {
	int ret = CPORTH_OK;

	if ((ret = __cporth_scan_quoted_string(program, loc, '"', CPORTH_TP_STR)) != 0) {
		goto out;
	}

	if (__cporth_scan_check_char(loc, 'c')) {
		CPORTH_PROGRAM_TOKEN_TOP->type = CPORTH_TP_CSTR;
	}

	out:
	return ret;
}

static int __cporth_scan_char (
		cporth_program *program,
		cporth_location *loc
) {
	int ret = CPORTH_OK;

	if ((ret = __cporth_scan_quoted_string(program, loc, '\'', CPORTH_TP_CHAR)) != 0) {
		goto out;
	}

	if (CPORTH_PROGRAM_TOKEN_TOP->size != 1) {
		cporth_err("Character type did not contain excactly one byte\n");
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan (
		cporth_program *program,
		cporth_scan_ctx *scan_ctx,
		cporth_location *loc,
		cporth_token_ref nest_begin_ref,
		int nest_level
) {
	int ret = 0;

	__cporth_scan_ignore_spaces(loc);

	CHECK_END();

	const cporth_location loc_orig = *loc;

	if (loc->length - loc->pos >= 2 && memcmp(loc->buf + loc->pos, "//", 2) == 0) {
		// Comment
		__cporth_scan_to_newline(loc);
	}
	else if (loc->length - loc->pos >= 1 && memcmp(loc->buf + loc->pos, "\"", 1) == 0) {
		loc->pos += 1;
		loc->col_pos += 1;
		if ((ret = __cporth_scan_str(program, loc)) != 0) {
			goto out;
		}
	}
	else if (loc->length - loc->pos >= 1 && memcmp(loc->buf + loc->pos, "\'", 1) == 0) {
		loc->pos += 1;
		loc->col_pos += 1;
		if ((ret = __cporth_scan_char(program, loc)) != 0) {
			goto out;
		}
	}
	else {
		if ((ret = __cporth_scan_to_space(loc)) != 0) {
			goto out;
		}

		assert(loc->pos != loc_orig.pos);

		struct cporth_scan_callback_data callback_data = {
			program,
			scan_ctx,
			loc,
			nest_begin_ref,
			nest_level,
			NULL,
			0,
			0
		};

		if ((ret = __cporth_scan_word (
				program,
				loc,
				&loc_orig,
				__cporth_scan_word_postprocess,
				NULL,
				&callback_data
		)) != 0) {
			goto out;
		}
	}

	CHECK_END();

	if (cporth_location_compare(&loc_orig, loc) == 0) {
		cporth_err("Syntax error\n");
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan_start (
		cporth_program *program,
		cporth_scan_ctx *scan_ctx,
		cporth_location *loc
) {
	cporth_token_ref nest_begin_ref = {0};
	return __cporth_scan (program, scan_ctx, loc, nest_begin_ref, 0);
}

static int __cporth_scan_file_to_program (
		cporth_program *program,
		cporth_scan_ctx *scan_ctx,
		const cporth_location *loc_parent,
		const char *filename
) {
	int ret = 0;

	cporth_file *file = NULL;

	// cporth_log("Scanning file '%s'...\n", filename);

	if ((ret = cporth_file_new (&file, filename)) != 0) {
		goto out;
	}

	cporth_location loc_static = {0};

	if (loc_parent == NULL) {
		// First file
		cporth_location_init(&loc_static, filename, CPORTH_LOC_NOTE_EMPTY);
	}
	else {
		// Included file.
		// loc will point to the "filename" part after include keyword
		// loc_parent will point to include keyword

		cporth_location *loc = CPORTH_PROGRAM_LOCATION_TOP;

		cporth_location_init(loc, filename, CPORTH_LOC_NOTE_FILE_INCLUSION);
		cporth_location_set_parent(loc, CPORTH_PROGRAM_LOCATION_TOPTOP_REF);

		if (cporth_program_nesting_count(program, CPORTH_PROGRAM_LOCATION_TOP_REF) > INCLUDE_NESTING_MAX) {
			cporth_err("Maximum number of nested includes reached (%u)\n", INCLUDE_NESTING_MAX);
			ret = CPORTH_ERR_HARD;
			goto out;
		}

		loc_static = *loc;
	}

	cporth_file_location_buf_set(&loc_static, file);

	while ((ret = __cporth_scan_start (program, scan_ctx, &loc_static)) == 0) {
		// Parse more
	}

	if (ret == CPORTH_EOF) {
		ret = CPORTH_OK;
	}
	else if (ret != CPORTH_OK) {
		cporth_program_err_l(&loc_static);
	}

	cporth_file_destroy(file);

	out:
	return ret;
}

int cporth_scan_file_to_program (
		cporth_program *program,
		const char *filename
) {
	int ret = 0;

	cporth_scan_ctx scan_ctx = {0};
	cporth_dict dict = {0};

	scan_ctx.dict = &dict;

	if ((ret = __cporth_scan_file_to_program(program, &scan_ctx, NULL, filename)) != 0) {
		goto out;
	}

	out:
	cporth_dict_private_clear(&dict);
	return ret;
}

static int __cporth_scan_until_end (
		cporth_program *program,
		cporth_scan_ctx *scan_ctx,
		cporth_location *loc,
		cporth_token_ref nest_begin_ref,
		int nest_level,
		short jump_return_set
) {
	int ret = CPORTH_OK;

	while ((ret = __cporth_scan (program, scan_ctx, loc, nest_begin_ref, nest_level)) == 0) {
		// Parse more
	}

	cporth_token *nest_begin = CPORTH_PROGRAM_REF_TO_TOKEN(nest_begin_ref);
	if (ret == CPORTH_EOF) {
		cporth_err("Keyword end not found after %s\n", (const char *) nest_begin->ptr);
		ret = CPORTH_ERR_HARD;
		goto out;
	}
	else if (ret == CPORTH_END) {
		cporth_token *end = CPORTH_PROGRAM_TOKEN_TOP;
		end->jump_return = jump_return_set;
		if (nest_begin->jump.i == 0) {
			cporth_token_jump_set(nest_begin, CPORTH_PROGRAM_TOKEN_TOP);
		}

		ret = CPORTH_OK;
	}

	out:
	return ret;
}

static int __cporth_scan_if (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	int ret = 0;

	if ((ret = __cporth_scan_until_end (program, scan_ctx, loc, CPORTH_PROGRAM_TOKEN_TOP_REF, callback_data->nest_level + 1, 0)) != 0) {
		goto out;
	}

	const cporth_token *end = CPORTH_PROGRAM_TOKEN_TOP;

	switch (end->kw->id) {
		case CPORTH_KW_END:
			break;
		default:
			cporth_err("end missing after if");
			ret = CPORTH_ERR_HARD;
			break;
	};

	// Parser will handle any else and if*

	out:
	return ret;
}

static int __cporth_scan_end (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	int ret = CPORTH_END;

	if (callback_data->nest_level == 0) {
		cporth_err("Unexpected end keyword\n");
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	const cporth_token *nest_begin = CPORTH_PROGRAM_REF_TO_TOKEN(callback_data->nest_begin);
	assert(nest_begin->kw != NULL);

	if ( cporth_token_is_kw_type(nest_begin, CPORTH_KW_MACRO) ||
	     cporth_token_is_kw_type(nest_begin, CPORTH_KW_MEMORY) ||
	     cporth_token_is_kw_type(nest_begin, CPORTH_KW_PROC) ||
	     cporth_token_is_kw_type(nest_begin, CPORTH_KW_CONST) ||
	     cporth_token_is_kw_type(nest_begin, CPORTH_KW_IF) ||
	     cporth_token_is_kw_type(nest_begin, CPORTH_KW_WHILE) ||
	     cporth_token_is_kw_type(nest_begin, CPORTH_KW_ASSERT) ||
	     cporth_token_is_kw_type(nest_begin, CPORTH_KW_CP_STRUCT)
	) {
		// OK
	}
	else {
		cporth_err("Unexpected end keyword after %s\n", nest_begin->kw->name);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan_while (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	int ret = CPORTH_OK;

	const cporth_token_ref at = CPORTH_PROGRAM_TOKEN_TOP_REF;

	if ((ret = __cporth_scan_until_end (program, scan_ctx, loc, CPORTH_PROGRAM_TOKEN_TOP_REF, callback_data->nest_level + 1, 0)) != 0) {
		goto out;
	}

	cporth_token *end = CPORTH_PROGRAM_TOKEN_TOP;

	switch (end->kw->id) {
		case CPORTH_KW_END:
			CPORTH_PROGRAM_TOKEN_JUMP_SET_FROM_REF(end, at);
			break;
		default:
			cporth_err("end missing after while");
			ret = CPORTH_ERR_HARD;
			break;
	};

	// Parser will move the jump set in while to the do token

	out:
	return ret;
}

static int __cporth_scan_here (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	int ret = CPORTH_OK;

	char *buf;
	int64_t len;

	CPORTH_PROGRAM_TOKEN_POP();

	if ((len = cporth_program_here_str_l(&buf, loc)) < 0) {
		cporth_err("Failed to create here string in %s\n", __func__);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	cporth_program_token_push (
			program,
			CPORTH_TP_STR,
			buf,
			(size_t) len,
			*loc,
			NULL,
			NULL,
			NULL
	);

	out:
	CPORTH_FREE_IF_NOT_NULL(buf);
	return ret;
}

static int __cporth_scan_ensure_non_reserved_name (
		const char *name
) {
	int found = 0;

	cporth_operator op = 0;
	cporth_operator_lookup(&found, &op, name);

	if (found) {
		cporth_err("The name %s is a reserved intrinsic\n", cporth_operator_names[op]);
		return CPORTH_ERR_HARD;
	}

	return CPORTH_OK;
}

static int __cporth_scan_push_keyword (
		cporth_dict *target_dict,
		const cporth_keyword *kw,
		size_t memory_size,
		size_t memory_pos,
		const cporth_dict *kw_dict
) {
	int ret = CPORTH_OK;

	if ((ret = __cporth_scan_ensure_non_reserved_name (kw->name)) != 0 ||
	    (ret = cporth_dict_kw_push_dynamic (target_dict, kw, memory_size, memory_pos, kw_dict)) != 0
	) {
		cporth_err("Failed to add keyword %s to dictionary\n", kw->name);
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan_dynamic_create_keyword (
		cporth_dict *target_dict,
		cporth_keyword_type kw_id,
		const cporth_dict *kw_dict,
		int (*kw_scan)(CPORTH_SCAN_KW_ARGS),
		const cporth_token_ref nest_begin,
		const char *name,
		cporth_analyze_data stack_data,
		size_t memory_size,
		size_t memory_pos,
		int64_t constant
) {
	const cporth_keyword kw_new = {
		kw_id,
		name,
		kw_scan,
		nest_begin,
		NULL,
		constant,
		stack_data,
		{0}
	};

	return __cporth_scan_push_keyword (
			target_dict,
			&kw_new,
			memory_size,
			memory_pos,
			kw_dict
	);
}

static int __cporth_scan_block_create_keyword (
		cporth_dict *target_dict,
		cporth_keyword_type kw_id,
		const cporth_dict *kw_dict,
		int (*kw_scan)(CPORTH_SCAN_KW_ARGS),
		const cporth_token_ref nest_begin,
		const char *name
) {
	const cporth_keyword kw_new = {
		kw_id,
		name,
		kw_scan,
		nest_begin,
		NULL,
		0,
		0,
		{0}
	};

	return __cporth_scan_push_keyword (
			target_dict,
			&kw_new,
			0,
			0,
			kw_dict
	);
}

static int __cporth_scan_block_update_keyword (
		cporth_dict *target_dict,
		const char *name,
		size_t memory_size
) {
	return cporth_dict_kw_update_dynamic (target_dict, name, memory_size);
}

static int __cporth_scan_expand_macro (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	int ret = CPORTH_OK;

	CPORTH_PROGRAM_TOKEN_POP();

	// Skip to first token after macro name and stop just before end
	cporth_token_ref ref = {kw->ref.i + 2};
	cporth_token_ref stop = {CPORTH_PROGRAM_REF_TO_TOKEN(kw->ref)->jump.i + kw->ref.i};
	for (; ref.i < stop.i; ref.i++) {
		if ((ret = cporth_program_token_copy_and_push (
				program,
				ref,
				loc
		)) != 0) {
			goto out;
		}
	}

	out:
	return ret;
}

static int __cporth_scan_expand_postprocess_kw_set (CPORTH_PROGRAM_KW_POSTPROCESS_ARGS) {
	POSTPROCESS_CALLBACK_DATA_SET();
	token->kw = kw;
	return CPORTH_OK;
}

static int __cporth_scan_expand_memory (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	void *ptr = kw->memory;

	return cporth_program_token_push (
			program,
			CPORTH_TP_PTR,
			&ptr,
			sizeof(ptr),
			*loc,
			__cporth_scan_expand_postprocess_kw_set,
			kw,
			arg
	);
}

static int __cporth_scan_expand_proc_postprocess (CPORTH_PROGRAM_KW_POSTPROCESS_ARGS) {
	POSTPROCESS_CALLBACK_DATA_SET();

	token->kw = kw;

	// Jump directly to proc keyword
	CPORTH_PROGRAM_TOKEN_JUMP_SET_FROM_REF(token, kw->ref);
	return CPORTH_OK;
}

static int __cporth_scan_expand_proc (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	CPORTH_PROGRAM_TOKEN_POP();

	return cporth_program_token_push (
			program,
			CPORTH_TP_CALL,
			NULL,
			0,
			*loc,
			__cporth_scan_expand_proc_postprocess,
			kw,
			callback_data
	);
}

static int __cporth_scan_expand_const (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	CPORTH_PROGRAM_TOKEN_POP();

	cporth_token_type tp = CPORTH_TP_INT;

	switch (kw->data) {
		case CPORTH_ANALYZE_DATA_INT:
			tp = CPORTH_TP_INT;
			break;
		case CPORTH_ANALYZE_DATA_BOOL:
			tp = CPORTH_TP_BOOL;
			break;
		case CPORTH_ANALYZE_DATA_PTR:
			tp = CPORTH_TP_PTR;
			break;
		case CPORTH_ANALYZE_DATA_NONE:
		case CPORTH_ANALYZE_DATA_GENERIC:
			assert(0);
	};

	return cporth_program_token_push (
			program,
			tp,
			&kw->constant,
			sizeof(kw->constant),
			*loc,
			__cporth_scan_expand_postprocess_kw_set,
			kw,
			arg
	);
}

static int __cporth_scan_reference_struct (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	cporth_token *token = CPORTH_PROGRAM_TOKEN_TOP;

	const void *memory_tmp = callback_data->memory + callback_data->memory_pos;

	return cporth_token_set (token, CPORTH_TP_PTR, kw, &memory_tmp, sizeof(memory_tmp));
}

static int __cporth_scan_instantiate_struct (
		cporth_scan_ctx *scan_ctx,
		cporth_program *program,
		const struct cporth_keyword *kw,
		const char *name,
		size_t memory_size,
		size_t memory_pos_offset
) {
	int ret = CPORTH_OK;

	cporth_dict kw_dict = {0};

	// Set parent after copying variables to avoid name collisions
	kw_dict.parent = &kw->dict;

	if ((ret = __cporth_scan_dynamic_create_keyword (
			scan_ctx->dict,
			kw->id,
			&kw_dict,
			__cporth_scan_reference_struct,
			CPORTH_PROGRAM_TOKEN_TOP_REF,
			name,
			CPORTH_ANALYZE_DATA_PTR,
			memory_size,
			memory_pos_offset,
			0
	)) != 0) {
		goto out;
	}

	out:
	cporth_dict_private_clear(&kw_dict);
	return ret;
}

static int __cporth_scan_expand_struct_postprocess (CPORTH_PROGRAM_KW_POSTPROCESS_ARGS) {
	POSTPROCESS_CALLBACK_DATA_SET();

	int ret = CPORTH_OK;

	if ((ret = __cporth_scan_instantiate_struct (
			scan_ctx,
			program,
			kw,
			(const char *) token->ptr,
			callback_data->memory_size,
			0
	)) != 0) {
		goto out;
	}	

	out:
	return ret;
}

static int __cporth_scan_expand_struct (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	int ret = CPORTH_OK;

	__cporth_scan_ignore_spaces(loc);

	const cporth_location loc_orig = *loc;

	if ((ret = __cporth_scan_to_space(loc)) != 0) {
		goto out;
	}

	if (loc->pos == loc_orig.pos) {
		cporth_err("Expected variable name for struct\n");
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	if ((ret = __cporth_scan_word (
			program,
			loc,
			&loc_orig,
			__cporth_scan_expand_struct_postprocess,
			kw,
			callback_data
	)) != 0) {
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan_expand_var (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	CPORTH_PROGRAM_TOKEN_POP();

	const void *memory_tmp = (callback_data->memory != NULL ? callback_data->memory + callback_data->memory_pos : kw->memory);

	return cporth_program_token_push (
			program,
			CPORTH_TP_PTR,
			&memory_tmp,
			sizeof(memory_tmp),
			*loc,
			NULL,
			kw,
			arg
	);
}

static int __cporth_scan_block (
		cporth_program *program,
		cporth_dict *target_dict,
		cporth_scan_ctx *scan_ctx,
		cporth_keyword_type id,
		cporth_dict *kw_dict,
		int (*kw_scan)(CPORTH_SCAN_KW_ARGS),
		const cporth_token_ref nest_begin,
		cporth_token *token,
		cporth_location *loc,
		const int nest_level,
		short set_jump_return
) {
	int ret = CPORTH_OK;

	if (token->type == CPORTH_TP_INT) {
		cporth_err ("Illegal keyword name '%li', keywords cannot look like numbers.\n", * (int64_t *) token->ptr);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	const cporth_token_ref at = CPORTH_PROGRAM_TOKEN_TOP_REF;

	// Create keyword first and update memory size afterwards. This
	// allows recursing in proc calls.
	if ((ret = __cporth_scan_block_create_keyword (
			target_dict,
			id,
			kw_dict,
			kw_scan,
			nest_begin,
			(const char *) token->ptr
	)) != 0) {
		goto out;
	}

	if ((ret = __cporth_scan_until_end (program, scan_ctx, loc, nest_begin, nest_level + 1, set_jump_return)) != 0) {
		goto out;
	}

	token = CPORTH_PROGRAM_REF_TO_TOKEN(at);

	size_t memory_size = 0;
	if (kw_dict != NULL) {
		for (cporth_dict_entry *de = kw_dict->first; de != NULL; de = de->next) {
			switch (de->kw->id) {
				case CPORTH_KW_CP_VAR:
					de->memory_pos = memory_size;
					memory_size += de->memory_size;
					break;
				case CPORTH_KW_CP_STRUCT:
					/* Add memory for instantiated structs only */
					if (de->kw->scan != __cporth_scan_reference_struct)
						break;
					if (de->memory_size == 0) {
						cporth_err("Memory size of %s not known at this time, possible self reference.\n", de->kw->name);
						ret = CPORTH_ERR_HARD;
						goto out;
					}
					de->memory_pos = memory_size;
					memory_size += de->memory_size;
					break;
				default:
					break;
			};
		}
		if (memory_size == 0) {
			memory_size = 1;
		}
	}

	if ((ret = __cporth_scan_block_update_keyword (
			target_dict,
			(const char *) token->ptr,
			memory_size
	)) != 0) {
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan_macro_postprocess (CPORTH_PROGRAM_KW_POSTPROCESS_ARGS) {
	POSTPROCESS_CALLBACK_DATA_SET();

	cporth_dict dict_dummy = {0};
	return __cporth_scan_block (
			program,
			scan_ctx->dict,
			scan_ctx,
			kw->id,
			&dict_dummy,
			__cporth_scan_expand_macro,
			callback_data->nest_begin,
			token,
			loc,
			callback_data->nest_level,
			0
	);
}

static int __cporth_scan_proc_postprocess (CPORTH_PROGRAM_KW_POSTPROCESS_ARGS) {
	POSTPROCESS_CALLBACK_DATA_SET();

	int ret = CPORTH_OK;

	cporth_scan_ctx scan_ctx_nest = *scan_ctx;

	cporth_dict dict = {0};
	dict.parent = scan_ctx->dict;
	scan_ctx_nest.dict = &dict;

	if ((ret = __cporth_scan_block (
			program,
			scan_ctx->dict,
			&scan_ctx_nest,
			kw->id,
			&dict,
			__cporth_scan_expand_proc,
			callback_data->nest_begin,
			token,
			loc,
			callback_data->nest_level,
			1
	)) != 0) {
		goto out;
	}

	cporth_token *end = CPORTH_PROGRAM_TOKEN_TOP;
	assert(end->kw->id == CPORTH_KW_END);
	end->jump_return = 1;

	out:
	cporth_dict_private_clear(&dict);
	return ret;
}

static int __cporth_scan_constant_eval_stack_data_callback (
		cporth_analyze_data data,
		void *arg
) {
	cporth_analyze_data *target = arg;
	*target = data;
	return CPORTH_OK;
}

struct cporth_scan_eval_run_callback_data {
	int64_t data;
};

static int __cporth_scan_constant_eval_run_callback (
		int64_t data,
		void *arg
) {
	struct cporth_scan_eval_run_callback_data *callback_data = arg;
	callback_data->data = data;
	return CPORTH_OK;
}

#define CPORTH_SCAN_CONSTANT_EVAL_CALLBACK_ARGS \
	cporth_scan_ctx *scan_ctx, const cporth_keyword *kw, const cporth_token_ref nest_begin, cporth_token *token, cporth_analyze_data stack_data, int64_t data

static int __cporth_scan_constant_eval (
		cporth_program *program,
		cporth_scan_ctx *scan_ctx,
		const cporth_keyword *kw,
		cporth_location *loc,
		const cporth_token_ref nest_begin,
		const int nest_level,
		int (*callback)(CPORTH_SCAN_CONSTANT_EVAL_CALLBACK_ARGS)
) {
	int ret = CPORTH_OK;

	const cporth_token_ref at = CPORTH_PROGRAM_TOKEN_TOP_REF;

	if ((ret = __cporth_scan_until_end(program, scan_ctx, loc, nest_begin, nest_level + 1, 0)) != 0) {
		goto out;
	}

	cporth_token *token = CPORTH_PROGRAM_REF_TO_TOKEN(at);
	cporth_token *begin = CPORTH_PROGRAM_REF_TO_TOKEN(nest_begin) + 2; // Start after keyword name
	cporth_token *end = program->tokens + program->tokens_length - 1;

	assert(end->kw != NULL && end->kw->id == CPORTH_KW_END);

	for (cporth_token *t = begin; t < end; t++) {
		int found = 0;
		switch (t->type) {
			case CPORTH_TP_INT:
				// OK
				break;
			case CPORTH_TP_WORD:
				cporth_parse_operator(&found, t);
				if (found) {
					break;
				}
				// Fallthrough
			default:
				cporth_err("Word %s not allowed in %s block\n", (const char *) t->ptr, kw->name);
				ret = CPORTH_ERR_HARD;
				goto out;
			
		};

		switch (t->op) {
			case CPORTH_OP_PLUS:
			case CPORTH_OP_MINUS:
			case CPORTH_OP_MUL:
			case CPORTH_OP_DIVMOD:
			case CPORTH_OP_MAX:
			case CPORTH_OP_DROP:
			case CPORTH_OP_EQ:
			case CPORTH_OP_CAST_PTR:
			case CPORTH_OP_CAST_INT:
			case CPORTH_OP_CAST_BOOL:
			case CPORTH_OP_OFFSET:
			case CPORTH_OP_RESET:
				break;
			default:
				cporth_err("Operator %s not allowed in %s block\n", (const char *) t->ptr, kw->name);
				ret = CPORTH_ERR_HARD;
				goto out;
		};
	}

	size_t stack_length = 0;
	cporth_analyze_data stack_data = 0;
	if ((ret = cporth_analyze_partial (
			&stack_length,
			program,
			scan_ctx,
			begin,
			end,
			__cporth_scan_constant_eval_stack_data_callback,
			&stack_data
	)) && ret != CPORTH_EOF) {
		assert(ret != 0);
		cporth_err("Evaluation of expression in %s block failed\n", kw->name);
		goto out;
	}

	assert(stack_length == 1);

	struct cporth_scan_eval_run_callback_data callback_data = {
		0
	};

	if ((ret = cporth_run_partial (
			program,
			scan_ctx,
			begin,
			__cporth_scan_constant_eval_run_callback,
			&callback_data
	)) != 0) {
		cporth_err("Evaluation of %s block expression failed return was %i\n", kw->name, ret);
		goto out;
	}

	if ((ret = callback (
			scan_ctx,
			kw,
			nest_begin,
			token,
			stack_data,
			callback_data.data
	)) != 0) {
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan_memory_postprocess_callback (CPORTH_SCAN_CONSTANT_EVAL_CALLBACK_ARGS) {
	int ret = CPORTH_OK;

	if (data <= 0) {
		cporth_err("Invalid memory size %" PRIi64 " for %s\n", data, kw->name);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	if (stack_data != CPORTH_ANALYZE_DATA_INT) {
		cporth_err("Incorrect data type in memory size expression. Expected INT.\n");
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	if ((ret = __cporth_scan_dynamic_create_keyword (
			scan_ctx->dict,
			kw->id,
			NULL,
			__cporth_scan_expand_memory,
			nest_begin,
			(const char *) token->ptr,
			stack_data,
			(size_t) data,
			0,
			0
	)) != 0) {
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan_memory_postprocess (CPORTH_PROGRAM_KW_POSTPROCESS_ARGS) {
	POSTPROCESS_CALLBACK_DATA_SET();

	(void)(token);

	return __cporth_scan_constant_eval (
			program,
			scan_ctx,
			kw,
			loc,
			callback_data->nest_begin,
			callback_data->nest_level,
			__cporth_scan_memory_postprocess_callback
	);
}

static int __cporth_scan_const_postprocess_callback (CPORTH_SCAN_CONSTANT_EVAL_CALLBACK_ARGS) {
	return __cporth_scan_dynamic_create_keyword (
			scan_ctx->dict,
			kw->id,
			NULL,
			__cporth_scan_expand_const,
			nest_begin,
			(const char *) token->ptr,
			stack_data,
			0,
			0,
			data
	);
}

static int __cporth_scan_const_postprocess (CPORTH_PROGRAM_KW_POSTPROCESS_ARGS) {
	POSTPROCESS_CALLBACK_DATA_SET();

	(void)(token);

	return __cporth_scan_constant_eval (
			program,
			scan_ctx,
			kw,
			loc,
			callback_data->nest_begin,
			callback_data->nest_level,
			__cporth_scan_const_postprocess_callback
	);
}

static int __cporth_scan_struct_postprocess (CPORTH_PROGRAM_KW_POSTPROCESS_ARGS) {
	POSTPROCESS_CALLBACK_DATA_SET();

	int ret = CPORTH_OK;

	cporth_scan_ctx scan_ctx_nest = *scan_ctx;

	cporth_dict dict = {0};
	dict.parent = scan_ctx->dict;
	scan_ctx_nest.dict = &dict;

	if ((ret = __cporth_scan_block (
			program,
			scan_ctx->dict,
			&scan_ctx_nest,
			kw->id,
			&dict,
			__cporth_scan_expand_struct,
			callback_data->nest_begin,
			token,
			loc,
			callback_data->nest_level,
			1
	)) != 0) {
		goto out;
	}

	

//	const cporth_token_ref at = CPORTH_PROGRAM_TOKEN_TOP_REF;

	out:
	cporth_dict_private_clear(&dict);
	return ret;
}

static int __cporth_scan_var_postprocess (CPORTH_PROGRAM_KW_POSTPROCESS_ARGS) {
	POSTPROCESS_CALLBACK_DATA_SET();

	int ret = CPORTH_OK;

	if ((ret = __cporth_scan_dynamic_create_keyword (
			scan_ctx->dict,
			CPORTH_KW_CP_VAR,
			NULL,
			__cporth_scan_expand_var,
			callback_data->nest_begin,
			(const char *) token->ptr,
			CPORTH_ANALYZE_DATA_INT,
			sizeof(int64_t),
			0,
			0
	)) != 0) {
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan_dynamic (
		const cporth_keyword *kw,
		int (*postprocess)(CPORTH_PROGRAM_KW_POSTPROCESS_ARGS),
		struct cporth_scan_callback_data *callback_data
) {
	cporth_program *program = callback_data->program;

	int ret = CPORTH_OK;

	if (callback_data->nest_level > 0) {
		if (callback_data->scan_ctx->dict != NULL && (
			kw->id == CPORTH_KW_MEMORY ||
			kw->id == CPORTH_KW_CONST ||
			kw->id == CPORTH_KW_PROC ||
			kw->id == CPORTH_KW_CP_STRUCT ||
			kw->id == CPORTH_KW_CP_VAR
		)) {
			// OK with these inside Proc or Struct
		}
		else {
			cporth_err("Keyword %s not allowed here\n", kw->name);
			ret = CPORTH_ERR_HARD;
			goto out;
		}
	}

	__cporth_scan_ignore_spaces(callback_data->loc);

	const cporth_location loc_orig = *callback_data->loc;

	if ((ret = __cporth_scan_to_space(callback_data->loc)) != 0) {
		goto out;
	}

	if (callback_data->loc->pos == loc_orig.pos) {
		cporth_err("Expected dynamic keyword name\n");
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	callback_data->nest_begin = CPORTH_PROGRAM_TOKEN_TOP_REF;

	if ((ret = __cporth_scan_word (
			program,
			callback_data->loc,
			&loc_orig,
			postprocess,
			kw,
			callback_data
	)) != 0) {
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan_macro (CPORTH_SCAN_KW_ARGS) {
	return __cporth_scan_dynamic (kw, __cporth_scan_macro_postprocess, arg);
}

static int __cporth_scan_memory (CPORTH_SCAN_KW_ARGS) {
	return __cporth_scan_dynamic (kw, __cporth_scan_memory_postprocess, arg);
}

static int __cporth_scan_proc (CPORTH_SCAN_KW_ARGS) {
	return __cporth_scan_dynamic (kw, __cporth_scan_proc_postprocess, arg);
}

static int __cporth_scan_const (CPORTH_SCAN_KW_ARGS) {
	return __cporth_scan_dynamic (kw, __cporth_scan_const_postprocess, arg);
}

static int __cporth_scan_struct (CPORTH_SCAN_KW_ARGS) {
	return __cporth_scan_dynamic (kw, __cporth_scan_struct_postprocess, arg);
}

static int __cporth_scan_var (CPORTH_SCAN_KW_ARGS) {
	return __cporth_scan_dynamic (kw, __cporth_scan_var_postprocess, arg);
}

static int __cporth_scan_ensure_quoted_string (
		cporth_program *program,
		cporth_location *loc,
		const cporth_keyword *kw
) {
	int ret = CPORTH_OK;

	__cporth_scan_ignore_spaces(loc);

	if (loc->length - loc->pos < 2 || memcmp(loc->buf + loc->pos, "\"", 1) != 0) {
		cporth_err("Expected quoted string after %s keyword\n", kw->name);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	loc->pos++;
	loc->col_pos++;

	if ((ret = __cporth_scan_str(program, loc)) != 0) {
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan_include (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	int ret = CPORTH_OK;

	if ((ret = __cporth_scan_ensure_quoted_string (program, loc, kw)) != 0) {
		goto out;
	}

	if ((ret = __cporth_scan_file_to_program (
			program,
			scan_ctx,
			loc,
			(const char *) CPORTH_PROGRAM_TOKEN_TOP_PTR
	)) != 0) {
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan_assert_callback (CPORTH_SCAN_CONSTANT_EVAL_CALLBACK_ARGS) {
	int ret = CPORTH_OK;

	(void)(scan_ctx);
	(void)(kw);
	(void)(nest_begin);

	if (token->type != CPORTH_TP_STR) {
		cporth_err("Expected quoted string after assert\n");
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	if (stack_data != CPORTH_ANALYZE_DATA_BOOL) {
		cporth_err("Expected boolean data in assertion\n");
		ret = CPORTH_ERR_HARD;
		goto out;
		
	}

	if (!data) {
		cporth_err("Assertion failed: %s\n", (const char *) token->ptr);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	out:
	return ret;
}

static int __cporth_scan_assert (CPORTH_SCAN_KW_ARGS) {
	SCAN_CALLBACK_DATA_SET();

	int ret = CPORTH_OK;

	const cporth_token_ref at = CPORTH_PROGRAM_TOKEN_TOP_REF;

	if ((ret = __cporth_scan_ensure_quoted_string (program, loc, kw)) != 0) {
		goto out;
	}

	if ((ret = __cporth_scan_constant_eval (
			program,
			scan_ctx,
			kw,
			loc,
			at,
			callback_data->nest_level + 1,
			__cporth_scan_assert_callback
	)) != 0) {
		goto out;
	}

	CPORTH_TOKEN_JUMP_SET(CPORTH_PROGRAM_REF_TO_TOKEN(at), CPORTH_PROGRAM_TOKEN_TOP);

	out:
	return ret;
}

static const cporth_keyword cporth_keywords_builtin[] = {
	{CPORTH_KW_IF,          "if",        __cporth_scan_if,         {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_IFSTAR,      "if*",       NULL,                     {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_ELSE,        "else",      NULL,                     {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_END,         "end",       __cporth_scan_end,        {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_WHILE,       "while",     __cporth_scan_while,      {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_DO,          "do",        NULL,                     {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_HERE,        "here",      __cporth_scan_here,       {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_MACRO,       "macro",     __cporth_scan_macro,      {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_INCLUDE,     "include",   __cporth_scan_include,    {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_MEMORY,      "memory",    __cporth_scan_memory,     {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_PROC,        "proc",      __cporth_scan_proc,       {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_CONST,       "const",     __cporth_scan_const,      {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_ASSERT,      "assert",    __cporth_scan_assert,     {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_INT,         "int",       NULL,                     {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_PTR,         "ptr",       NULL,                     {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_BOOL,        "bool",      NULL,                     {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_BIKESHEDDER, "--",        NULL,                     {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_IN,          "in",        NULL,                     {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_CP_STRUCT,   "struct",    __cporth_scan_struct,     {0}, NULL, 0, 0, {0}},
	{CPORTH_KW_CP_VAR,      "var",       __cporth_scan_var,        {0}, NULL, 0, 0, {0}},
	{0, NULL, NULL, {0}, NULL, 0, 0, {0}}
};

int cporth_scan_init (void) {
	int ret = CPORTH_OK;

	int i = 0;
	const cporth_keyword *kw = &cporth_keywords_builtin[i];
	while (kw->name != NULL) {
		if ((ret = cporth_dict_kw_push_global(kw)) != 0) {
			goto out;
		}

		kw = &cporth_keywords_builtin[++i];
	}

	out:
	return ret;
}

void cporth_scan_cleanup (void) {
	cporth_dict_cleanup();
}
