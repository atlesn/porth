#include "program_struct.h"
#include "token.h"
#include "error.h"
#include "program.h"
#include "log.h"

// Keywords which change in preparation of analyze step
static const cporth_keyword cporth_parse_keywords_builtin[] = {
	{CPORTH_KW_SP_ORELSE, "else", NULL, {0}, NULL, 0, 0, {0}}
};

#define CPORTH_PARSE_KW_ORELSE  (&cporth_parse_keywords_builtin[0])

void cporth_parse_operator (
		int *found,
		cporth_token *t
) {
	*found = 0;

	if (t->ptr == NULL)
		return;

	cporth_operator_lookup(found, &t->op, (const char *) t->ptr);
}

#define CPORTH_PARSE_ARGS                                      \
    cporth_token **stop,                                       \
    cporth_program *program,                                   \
    cporth_token *t,                                           \
    cporth_token * const prev                                  \

static int __cporth_parse_at (CPORTH_PARSE_ARGS);
static int __cporth_parse_at_struct (CPORTH_PARSE_ARGS);
static int __cporth_parse_at_proc_args (CPORTH_PARSE_ARGS);

static int __cporth_parse_struct (
		cporth_token **t,
		cporth_program *program
) {
	int ret = CPORTH_OK;

	const cporth_token *stop_expected = NULL;

	if (CPORTH_TOKEN_JUMPOF(*t) == NULL) {
		// Instantiation, next token is variable name
		(*t)++;
		goto out;
	}

	stop_expected = CPORTH_TOKEN_JUMPOF(*t);

	if ((ret = __cporth_parse_at_struct (
			t,
			program,
			(*t) + 2, /* Start after struct name */
			*t
	)) != 0) {
		goto out;
	}

	assert ((*t)->kw != NULL && (*t)->kw->id == CPORTH_KW_END && stop_expected == (*t));

	out:
	return ret;
}

static int __cporth_parse_proc (
		cporth_token **t,
		cporth_program *program
) {
	int ret = CPORTH_OK;

	cporth_token *t_in = NULL;
	cporth_token *t_end = NULL;
	const cporth_token *stop_expected = NULL;

	if ((ret = __cporth_parse_at_proc_args (
			&t_in,
			program,
			(*t) + 2 /* Start after proc name */,
			*t
	)) != 0) {
		goto out;
	}

	assert(t_in->kw != NULL && t_in->kw->id == CPORTH_KW_IN);

	// Jump of proc is set to end during scanning
	stop_expected = CPORTH_TOKEN_JUMPOF(*t);
	assert(stop_expected->kw != NULL && stop_expected->kw->id == CPORTH_KW_END);

	if ((ret = __cporth_parse_at (
			&t_end,
			program,
			t_in + 1 /* Start after in */,
			*t /* Pass proc here to pass assertion when reaching END keyword */
	)) != 0) {
		goto out;
	}

	assert (stop_expected == t_end);

	// Proc jumps to in and then in jumps to end.
	CPORTH_TOKEN_JUMP_SET(*t, t_in);
	CPORTH_TOKEN_JUMP_SET(t_in, t_end);

	*t = t_end;

	out:
	return ret;
}

static int __cporth_parse_at_struct (CPORTH_PARSE_ARGS) {
	int ret = CPORTH_OK;

	*stop = NULL;

	cporth_token * const next_end = prev != NULL ? CPORTH_TOKEN_JUMPOF(prev) : NULL;

	cporth_token *end = program->tokens + program->tokens_length;

	for (; t < end; t++) {
		*stop = t;

		switch (t->type) {
			case CPORTH_TP_WORD:
				cporth_err("Words not allowed in struct ('%s' gived)\n", (const char *) t->ptr);
				goto out_err_descend;
			case CPORTH_TP_INT:
			case CPORTH_TP_BOOL:
			case CPORTH_TP_PTR:
			case CPORTH_TP_STR:
			case CPORTH_TP_CSTR:
			case CPORTH_TP_CHAR:
				cporth_err("Operands not allowed in struct\n");
				goto out_err_descend;
			case CPORTH_TP_CALL:
				cporth_err("Procedure calls not allowed in struct\n");
				goto out_err_descend;
			case CPORTH_TP_KEYWORD:
				switch (t->kw->id) {
					case CPORTH_KW_ASSERT:
					case CPORTH_KW_CONST:
					case CPORTH_KW_MACRO:
					case CPORTH_KW_MEMORY:
						t = CPORTH_TOKEN_JUMPOF_OR_SELF(t);
						break;
					case CPORTH_KW_CP_VAR:
						t++; /* Skip name */
						break;
					case CPORTH_KW_CP_STRUCT:
						if ((ret = __cporth_parse_struct (
								&t,
								program
						)) != 0) {
							goto out;
						}
						break;
					case CPORTH_KW_PROC:
						if ((ret = __cporth_parse_proc (
								&t,
								program
						)) != 0) {
							goto out;
						}
						break;
					case CPORTH_KW_END:
						assert(t == next_end);
						goto out;
					default:
						cporth_err("Keyword %s not allowed in struct\n", t->kw->name);
						goto out_err_descend;
				};
				break;
		};
	}

	goto out;
	out_err_descend:
		cporth_program_err_descend(program, CPORTH_PROGRAM_TOKEN_TO_LREF(t));
		ret = CPORTH_ERR_HARD;
	out:
		return ret;
}

static int __cporth_parse_at_proc_args (CPORTH_PARSE_ARGS) {
	(void)(prev);

	int ret = CPORTH_OK;

	cporth_token *colon = NULL;

	cporth_token *end = program->tokens + program->tokens_length;
	for (; t < end; t++) {
		*stop = t;

		switch (t->type) {
			case CPORTH_TP_CSTR:
			case CPORTH_TP_STR:
				break;
			case CPORTH_TP_KEYWORD:
				switch (t->kw->id) {
					case CPORTH_KW_INT:
					case CPORTH_KW_PTR:
					case CPORTH_KW_BOOL:
						break;
					case CPORTH_KW_BIKESHEDDER:
						if (colon != NULL) {
							cporth_err("Extra colon ':' found in proc declaration\n");
							goto out_err_descend;
						}
						colon = t;
						break;
					case CPORTH_KW_IN:
						goto out;
					default:
						cporth_err("Keyword %s not allowed in proc declaration\n", t->kw->name);
						goto out_err_descend;
				};
				break;
			case CPORTH_TP_WORD:
			case CPORTH_TP_INT:
				cporth_err("Word %s not allowed in proc declaration\n", (const char *) t->ptr);
				goto out_err_descend;
			default:
				cporth_err("Invalid word in proc declaration\n");
				goto out_err_descend;
		};

	}


	goto out;
	out_err_descend:
		cporth_program_err_descend(program, CPORTH_PROGRAM_TOKEN_TO_LREF(t));
		ret = CPORTH_ERR_HARD;
	out:
		return ret;
}

static int __cporth_parse_at (CPORTH_PARSE_ARGS) {
	int ret = CPORTH_OK;

	*stop = NULL;

	cporth_token *stop_tmp = NULL;

	cporth_token *prev_do = NULL;
	cporth_token *prev_else = NULL;
	cporth_token *prev_ifstar = NULL;
	cporth_token * const next_end = prev != NULL ? CPORTH_TOKEN_JUMPOF(prev) : NULL;
	const cporth_token *stop_expected = NULL;

	cporth_token *end = program->tokens + program->tokens_length;
	for (; t < end; t++) {
		switch (t->type) {
			case CPORTH_TP_WORD:
				{
					int found = 0;
					cporth_parse_operator(&found, t);
					if (!found) {
						cporth_err("Unresolvable word %s\n", (const char *) t->ptr);
						cporth_program_err_descend(program, CPORTH_PROGRAM_TOKEN_TO_LREF(t));
						ret = CPORTH_ERR_HARD;
						goto out;
					}
				}
				break;
			case CPORTH_TP_INT:
			case CPORTH_TP_BOOL:
			case CPORTH_TP_PTR:
			case CPORTH_TP_STR:
			case CPORTH_TP_CSTR:
			case CPORTH_TP_CHAR:
			case CPORTH_TP_CALL:
				break;
			case CPORTH_TP_KEYWORD:
				switch (t->kw->id) {
					case CPORTH_KW_CONST:
					case CPORTH_KW_MACRO:
					case CPORTH_KW_MEMORY:
					case CPORTH_KW_HERE:
					case CPORTH_KW_ASSERT:
						t = CPORTH_TOKEN_JUMPOF_OR_SELF(t);
						break;
					case CPORTH_KW_INCLUDE:
					case CPORTH_KW_CP_VAR:
						t++;
						break;
					case CPORTH_KW_INT:
					case CPORTH_KW_PTR:
					case CPORTH_KW_BOOL:
					case CPORTH_KW_BIKESHEDDER:
					case CPORTH_KW_IN:
						goto out_err_unexpected;
					case CPORTH_KW_CP_STRUCT:
						if ((ret = __cporth_parse_struct (
								&t,
								program
						)) != 0) {
							goto out;
						}
						break;
					case CPORTH_KW_PROC:
						if ((ret = __cporth_parse_proc (
								&t,
								program
						)) != 0) {
							goto out;
						}
						break;
					case CPORTH_KW_WHILE:
					case CPORTH_KW_IF:
						assert(t->jump.i > 0);

						stop_expected = CPORTH_TOKEN_JUMPOF(t);

						if ((ret = __cporth_parse_at (
								&stop_tmp,
								program,
								t + 1,
								t
						)) != 0) {
							goto out;
						}

						if (t->kw->id == CPORTH_KW_WHILE && t->jump.i != 0) {
							cporth_err("Missing do after while\n");
							goto out_err_descend;
						}

						assert (stop_tmp->kw != NULL && stop_tmp->kw->id == CPORTH_KW_END && stop_expected == stop_tmp);

						if (t->kw->id == CPORTH_KW_IN) {
							*stop = stop_tmp;
							goto out;
						}

						t = stop_tmp;

						break;
					case CPORTH_KW_ELSE:
						if ((prev_else != NULL && prev_ifstar == NULL) || prev == NULL || (prev->kw->id != CPORTH_KW_IF)) {
							goto out_err_unexpected;
						}

						if (prev_ifstar != NULL) {
							CPORTH_TOKEN_JUMP_SET(prev_ifstar, t);
						}
						else {
							CPORTH_TOKEN_JUMP_SET(prev, t);
						}

						CPORTH_TOKEN_JUMP_SET(t, next_end);

						prev_else = t;
						prev_ifstar = NULL;

						break;
					case CPORTH_KW_IFSTAR:
						if (prev_else == NULL || prev_ifstar != NULL) {
							cporth_err("Unexpected %s\n", t->kw->name);
							goto out_err_unexpected;
						}

						prev_ifstar = t;

						prev_else->kw = CPORTH_PARSE_KW_ORELSE;

						break;
					case CPORTH_KW_DO:
						assert(t->jump.i == 0);
						if (prev_do != NULL || prev == NULL || (prev->kw->id != CPORTH_KW_WHILE)) {
							goto out_err_unexpected;
						}

						CPORTH_TOKEN_JUMP_SET_FROM(t, prev);
						CPORTH_TOKEN_JUMP_UNSET(prev);

						prev_do = t;

						break;
					case CPORTH_KW_END:
						assert(t == next_end);
						if (prev_ifstar != NULL) {
							CPORTH_TOKEN_JUMP_SET(prev_ifstar, t);
						}
						else if (prev_else != NULL) {
							CPORTH_TOKEN_JUMP_SET(prev_else, t);
						}
						else if (prev_do != NULL) {
							CPORTH_TOKEN_JUMP_SET(prev_do, t);
						}
						*stop = t;
						goto out;
					case CPORTH_KW_SP_ORELSE:
						assert(0);
				};
				break;
		};
	}

	*stop = t;

	goto out;
	out_err_unexpected:
		cporth_err("Unexpected %s\n", t->kw->name);
	out_err_descend:
		cporth_program_err_descend(program, CPORTH_PROGRAM_TOKEN_TO_LREF(t));
		ret = CPORTH_ERR_HARD;
	out:
		return ret;

}

int cporth_parse (
		cporth_program *program
) {
	int ret = CPORTH_OK;

	cporth_token *stop = NULL;

	if ((ret = __cporth_parse_at (&stop, program, program->tokens, NULL)) != 0) {
		goto out;
	}

	assert(stop == program->tokens + program->tokens_length);

	out:
	return ret;
}
