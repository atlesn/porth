#ifndef CPORTH_RUN_H
#define CPORTH_RUN_H

#include <stdint.h>

typedef struct cporth_program cporth_program;
typedef struct cporth_token cporth_token;
typedef struct cporth_scan_ctx cporth_scan_ctx;

int cporth_run (
		const cporth_program *program,
		int argc,
		const char **argv
);
int cporth_run_partial (
		const cporth_program *program,
		cporth_scan_ctx *scan_ctx,
		const cporth_token *begin,
		int (*callback)(int64_t data, void *arg),
		void *arg
);

#endif /* CPORTH_RUN_H */
