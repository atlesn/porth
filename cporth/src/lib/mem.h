#ifndef CPORTH_MEM_H
#define CPORTH_MEM_H

#include <stddef.h>

typedef struct cporth_mem {
	size_t size;
	size_t pos;
	void *data;
} cporth_mem;

void *cporth_mem_get (
		cporth_mem *mem,
		size_t size
);

int cporth_mem_init (
		cporth_mem *mem,
		size_t size
);

void cporth_mem_cleanup (
		cporth_mem *mem
);

#endif /* CPORTH_MEM_H */
