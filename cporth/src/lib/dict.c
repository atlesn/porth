#include <stddef.h>
#include <stdio.h>

#include "allocate.h"
#include "location.h"
#include "dict.h"
#include "error.h"
#include "log.h"
#include "util.h"
#include "token.h"

typedef struct cporth_dict_entry cporth_dict_entry;

#define CPORTH_KEYWORD_MAX (1024+16)
#define CPORTH_KEYWORD_DYNAMIC_MAX 1024

static const cporth_keyword **cporth_keywords = NULL;
static size_t cporth_keywords_length = 0;

static cporth_keyword *cporth_keywords_dynamic = NULL;
static size_t cporth_keywords_dynamic_length = 0;

void cporth_dict_private_clear (cporth_dict *dict) {
	cporth_dict_entry *entry = dict->first;
	while (entry != NULL) {
		cporth_dict_entry *next = entry->next;
		cporth_free(entry);
		entry = next;
	}
	dict->first = NULL;
	dict->last = NULL;
}

static int __cporth_dict_private_push (cporth_dict *dict, const cporth_keyword *kw, size_t memory_size, size_t memory_pos) {
	cporth_dict_entry *entry;
	if ((entry = cporth_allocate_zero(sizeof(*entry))) == NULL) {
		cporth_err ("Failed to cporth_allocate memory in %s\n", __func__);
		return CPORTH_ERR_HARD;
	}

	entry->kw = kw;
	entry->memory_size = memory_size;
	entry->memory_pos = memory_pos;

	if (dict->last)
		dict->last->next = entry;
	else
		dict->first = entry;

	dict->last = entry;

	return CPORTH_OK;
}

void __cporth_dict_kw_cleanup (cporth_keyword *kw) {
	if (kw->memory != NULL) {
		cporth_free(kw->memory);
	}
	cporth_dict_private_clear (&kw->dict);
}

static int __cporth_dict_set_from (cporth_dict *dst, const cporth_dict *src) {
	int ret = CPORTH_OK;

	cporth_dict_private_clear(dst);

	const cporth_dict_entry *entry = src->first;
	while (entry != NULL) {
		if ((ret = __cporth_dict_private_push (dst, entry->kw, entry->memory_size, entry->memory_pos)) != 0) {
			goto out;
		}
		entry = entry->next;
	}

	dst->parent = src->parent;

	out:
	return ret;
}

cporth_dict_entry *__cporth_dict_lookup_no_parent (const cporth_dict *dict, const char *name) {
	if (dict == NULL)
		return NULL;

	cporth_dict_entry *entry = dict->first;
	while (entry != NULL) {
		const cporth_keyword *kw = entry->kw;
		if (strcmp(name, kw->name) == 0) {
			return entry;
		}
		entry = entry->next;
	}

	return NULL;
}

const cporth_dict_entry *cporth_dict_lookup_no_parent (const cporth_dict *dict, const char *name) {
	return __cporth_dict_lookup_no_parent(dict, name);
}

const cporth_dict_entry *cporth_dict_lookup (const cporth_dict *dict, const char *name) {
	while (dict != NULL) {
		const cporth_dict_entry *entry;
		if ((entry = cporth_dict_lookup_no_parent (dict, name)) != NULL)
			return entry;
		dict = dict->parent;
	}

	return NULL;
}

const cporth_keyword *cporth_dict_kw_lookup (const cporth_dict *dict, const char *name) {
	while (dict != NULL) {
		const cporth_dict_entry *entry;
		if ((entry = cporth_dict_lookup_no_parent (dict, name)) != NULL)
			return entry->kw;
		dict = dict->parent;
	}

	for (size_t i = 0; i < cporth_keywords_length; i++) {
		if (strcmp(name, cporth_keywords[i]->name) == 0) {
			return cporth_keywords[i];
		}
	}

	return NULL;
}

int cporth_dict_kw_push_global (const cporth_keyword *kw) {
	int ret = CPORTH_OK;

	// Caller must ensure that the lifetime of kw is not shorter than the lifetime of dicter

	assert(cporth_keywords_length < CPORTH_KEYWORD_MAX);

	if (cporth_keywords == NULL) {
		if ((cporth_keywords = cporth_allocate(sizeof(void *) * CPORTH_KEYWORD_MAX)) == NULL) {
			cporth_err("Failed to allocate memory in %s\n", __func__);
			ret = CPORTH_ERR_HARD;
			goto out;
		}
	}

	cporth_keywords[cporth_keywords_length++] = kw;

	out:
	return ret;
}

static int __cporth_dict_kw_allocate_dynamic (
		const cporth_keyword *kw,
		size_t memory_size,
		const cporth_dict *kw_dict,
		int (*callback)(cporth_keyword *kw, void *arg),
		void *callback_arg
) {
	int ret = CPORTH_OK;

	assert(cporth_keywords_dynamic_length < CPORTH_KEYWORD_DYNAMIC_MAX);

	if (cporth_keywords_dynamic == NULL) {
		if ((cporth_keywords_dynamic = cporth_allocate(sizeof(*cporth_keywords_dynamic) * CPORTH_KEYWORD_DYNAMIC_MAX)) == NULL) {
			cporth_err("Failed to allocate memory in %s\n", __func__);
			ret = CPORTH_ERR_HARD;
			goto out;
		}
	}

	{
		cporth_keyword kw_new = *kw;

		if (kw_dict != NULL) {
			if ((ret = __cporth_dict_set_from (&kw_new.dict, kw_dict)) != 0) {
				goto out;
			}
		}

		if (memory_size != 0) {
			if ((kw_new.memory = cporth_allocate_zero(memory_size)) == NULL) {
				cporth_err("Failed to allocate memory for data (%lu bytes) in %s\n", memory_size, __func__);
				goto out;
			}
		}
		else {
			kw_new.memory = NULL;
		}

		cporth_keywords_dynamic[cporth_keywords_dynamic_length++] = kw_new;

		if ((ret = callback(&cporth_keywords_dynamic[cporth_keywords_dynamic_length - 1], callback_arg)) != 0) {
			goto out_pop;
		}
	}

	goto out;
	out_pop:
		__cporth_dict_kw_cleanup(&cporth_keywords_dynamic[--cporth_keywords_dynamic_length]);
	out:
		return ret;
}

static int __cporth_dict_kw_push_global_dynamic_callback (cporth_keyword *kw, void *arg) {
	(void)(arg);
	return cporth_dict_kw_push_global(kw);
}

struct cporth_dict_push_private_dynamic_callback_data {
	cporth_dict *dict;
	size_t memory_size;
	size_t memory_pos;
};

static int __cporth_dict_kw_push_private_dynamic_callback (cporth_keyword *kw, void *arg) {
	struct cporth_dict_push_private_dynamic_callback_data *callback_data = arg;
	return __cporth_dict_private_push(callback_data->dict, kw, callback_data->memory_size, callback_data->memory_pos);
}

int cporth_dict_kw_push_dynamic_unchecked (
		cporth_dict *target_dict,
		const cporth_keyword *kw,
		size_t memory_size,
		size_t memory_pos,
		const cporth_dict *kw_dict
) {
	// Caller need not ensure persistense of kw, but pointer members must persist

	assert(kw->name != NULL && *kw->name != '\0');

	struct cporth_dict_push_private_dynamic_callback_data private_callback_data = {
		target_dict,
		memory_size,
		memory_pos
	};

	return (target_dict != NULL
		? __cporth_dict_kw_allocate_dynamic(kw, memory_size, kw_dict, __cporth_dict_kw_push_private_dynamic_callback, &private_callback_data)
		: __cporth_dict_kw_allocate_dynamic(kw, memory_size, kw_dict, __cporth_dict_kw_push_global_dynamic_callback, NULL)
	);
}
		
int cporth_dict_kw_push_dynamic (
		cporth_dict *target_dict,
		const cporth_keyword *kw,
		size_t memory_size,
		size_t memory_pos,
		const cporth_dict *kw_dict
) {
	if (cporth_dict_kw_lookup(target_dict, kw->name) != NULL) {
		cporth_err("Duplicate keyword name %s\n", kw->name);
		return CPORTH_ERR_HARD;
	}

	return cporth_dict_kw_push_dynamic_unchecked (target_dict, kw, memory_size, memory_pos, kw_dict);
}
	
int cporth_dict_kw_update_dynamic (
		cporth_dict *target_dict,
		const char *name,
		size_t memory_size
) {
	int ret = CPORTH_OK;
	cporth_dict_entry *de = __cporth_dict_lookup_no_parent (target_dict, name);
	assert(de != NULL && de->kw != NULL);

	// Case away const
	cporth_keyword *kw = (cporth_keyword *) de->kw;

	CPORTH_FREE_IF_NOT_NULL(kw->memory);
	kw->memory = NULL;

	if (memory_size > 0 && (((cporth_keyword *) de->kw)->memory = cporth_allocate_zero (memory_size)) == NULL) {
		cporth_err("Failed to allocate memory for data (%lu bytes) in %s\n", memory_size, __func__);
		goto out;
	}

	de->memory_size = memory_size;

	out:
	return ret;
}

void cporth_dict_cleanup (void) {
	if (cporth_keywords_dynamic != NULL) {
		for (size_t i = 0; i < cporth_keywords_dynamic_length; i++) {
			__cporth_dict_kw_cleanup(cporth_keywords_dynamic + i);
		}
	}
	CPORTH_FREE_IF_NOT_NULL(cporth_keywords);
	CPORTH_FREE_IF_NOT_NULL(cporth_keywords_dynamic);
}
