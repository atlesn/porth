#define _GNU_SOURCE /* For syscall() and environ */
#include <unistd.h>
#include <sys/syscall.h>

#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <inttypes.h>

#include "run.h"
#include "program_struct.h"
#include "allocate.h"
#include "error.h"
#include "log.h"
#include "mem.h"
#include "operator.h"
#include "token.h"
#include "util.h"
#include "scan_ctx.h"

#define CPORTH_RUN_STACK_EXTRA 16

typedef struct cporth_run_stack_frame {
	union {
		int64_t data;
		void *ptr;
		char bytes[8];
	};
} cporth_run_stack_frame;

typedef struct cporth_run_stack {
	size_t length;
	size_t size;
	cporth_run_stack_frame frames[1];
} cporth_run_stack;

static int __cporth_run_stack_new (cporth_run_stack **stack, size_t size) {
	int ret = CPORTH_OK;

	*stack = NULL;

	cporth_run_stack *stack_tmp = NULL;

	if ((stack_tmp = cporth_allocate(sizeof(*stack_tmp) - sizeof(stack_tmp->frames) + sizeof(stack_tmp->frames) * size)) == NULL) {
		cporth_err("Failed to allocate memory for stack in %s\n", __func__);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	stack_tmp->size = size;
	stack_tmp->length = 0;

	*stack = stack_tmp;

	out:
	return ret;
}

static void __cporth_run_stack_destroy (cporth_run_stack *stack) {
	cporth_free(stack);
}

#define POP() \
    (stack->frames + --stack->length)

#define VOIDPOP() \
    (--stack->length)

#define TOP \
    (stack->frames + stack->length - 1)

#define TOPINT \
    (TOP->data)

static void __cporth_run_stack_push_int (
		cporth_run_stack *stack,
		int64_t data
) {
	assert(stack->length < stack->size);
	(stack->frames + stack->length++)->data = data;
}

static void __cporth_run_stack_push_ptr (
		cporth_run_stack *stack,
		void *ptr
) {
	assert(stack->length < stack->size);
	(stack->frames + stack->length++)->ptr = ptr;
}

static int64_t __cporth_run_stack_pop_int (
		cporth_run_stack *stack
) {
	assert(stack->length > 0);
	return POP()->data;
}

static void *__cporth_run_stack_pop_ptr (
		cporth_run_stack *stack
) {
	assert(stack->length > 0);
	return POP()->ptr;
}

#define PUSHINT(i) \
    __cporth_run_stack_push_int(stack, i)

#define PUSHPTR(p) \
    __cporth_run_stack_push_ptr(stack, p)

#define POPINT(p) \
    __cporth_run_stack_pop_int(stack)

#define POPPTR(p) \
    __cporth_run_stack_pop_ptr(stack)

typedef struct cporth_run_args {
	int64_t argc;
	void *argv[1];
} cporth_run_args;

static int __cporth_run_allocate_args (
		cporth_mem *mem,
		int argc,
		const char **argv
) {
	int ret = CPORTH_OK;

	cporth_run_args *args;

	const size_t args_size = sizeof(*args) - sizeof(args->argv) + sizeof(args->argv) * argc;

	size_t mem_size = args_size;
	for (int i = 0; i < argc; i++) {
		mem_size += strlen(argv[i]) + 1;
	}

	if ((ret = cporth_mem_init (mem, mem_size)) != 0) {
		goto out;
	}

	if ((args = cporth_mem_get(mem, args_size)) == NULL) {
		goto out;
	}

	args->argc = argc;

	for (int i = 0; i < argc; i++) {
		const size_t arg_size = 1 + strlen(argv[i]);
		void * const arg_mem = cporth_mem_get(mem, arg_size);
		assert(arg_mem != NULL);
		memcpy(arg_mem, argv[i], arg_size);
		args->argv[i] = arg_mem;
	}

	out:
	return ret;
}

static  cporth_run_args *__cporth_run_get_args (
		cporth_mem *mem
) {
	return mem->data;
}

static int __cporth_run_word (
		const cporth_token **token,
		cporth_scan_ctx *scan_ctx,
		cporth_run_stack *stack,
		cporth_mem *mem
) {
	int ret = CPORTH_OK;

	cporth_run_args *args = __cporth_run_get_args(mem);

	int64_t a, b, c, d, e, f, g;
	void *ptr;
	char buf[32];

	switch ((*token)->op) {
		case CPORTH_OP_PLUS:
			b = POPINT();
			a = POPINT();
			PUSHINT(a + b);
			break;
		case CPORTH_OP_MINUS:
			b = POPINT();
			a = POPINT();
			PUSHINT(a - b);
			break;
		case CPORTH_OP_MUL:
			b = POPINT();
			a = POPINT();
			PUSHINT(a * b);
			break;
		case CPORTH_OP_DIVMOD:
			b = POPINT();
			a = POPINT();
			PUSHINT(a / b);
			PUSHINT(a % b);
			break;
		case CPORTH_OP_MAX:
			b = POPINT();
			a = POPINT();
			PUSHINT(a > b ? a : b);
			break;
		case CPORTH_OP_EQ:
			b = POPINT();
			a = POPINT();
			PUSHINT(a == b);
			break;
		case CPORTH_OP_GT:
			b = POPINT();
			a = POPINT();
			PUSHINT(a > b);
			break;
		case CPORTH_OP_LT:
			b = POPINT();
			a = POPINT();
			PUSHINT(a < b);
			break;
		case CPORTH_OP_GE:
			b = POPINT();
			a = POPINT();
			PUSHINT(a >= b);
			break;
		case CPORTH_OP_LE:
			b = POPINT();
			a = POPINT();
			PUSHINT(a <= b);
			break;
		case CPORTH_OP_NE:
			b = POPINT();
			a = POPINT();
			PUSHINT(a != b);
			break;
		case CPORTH_OP_SHR:
			b = POPINT();
			a = POPINT();
			PUSHINT(a >> b);
			break;
		case CPORTH_OP_SHL:
			b = POPINT();
			a = POPINT();
			PUSHINT(a << b);
			break;
		case CPORTH_OP_OR:
			b = POPINT();
			a = POPINT();
			PUSHINT(a | b);
			break;
		case CPORTH_OP_AND:
			b = POPINT();
			a = POPINT();
			PUSHINT(a & b);
			break;
		case CPORTH_OP_NOT:
			a = POPINT();
			PUSHINT(~a);
			break;
		case CPORTH_OP_PRINT:
			sprintf(buf, "%" PRIi64 "\n", POPINT());
			write(STDOUT_FILENO, buf, strlen(buf));
			break;
		case CPORTH_OP_DUP:
			PUSHINT(TOPINT);
			break;
		case CPORTH_OP_SWAP:
			b = POPINT();
			a = POPINT();
			PUSHINT(b);
			PUSHINT(a);
			break;
		case CPORTH_OP_DROP:
			VOIDPOP();
			break;
		case CPORTH_OP_OVER:
			b = POPINT();
			a = POPINT();
			PUSHINT(a);
			PUSHINT(b);
			PUSHINT(a);
			break;
		case CPORTH_OP_ROT:
			c = POPINT();
			b = POPINT();
			a = POPINT();
			PUSHINT(b);
			PUSHINT(c);
			PUSHINT(a);
			break;
		case CPORTH_OP_LOAD8:
			PUSHINT((* (int8_t *) POPPTR()) & 0xff);
			break;
		case CPORTH_OP_LOAD16:
			PUSHINT((* (int16_t *) POPPTR()) & 0xffff);
			break;
		case CPORTH_OP_LOAD32:
			PUSHINT((* (int32_t *) POPPTR()) & 0xffffffff);
			break;
		case CPORTH_OP_LOAD64:
			ptr = POPPTR();
			PUSHINT(* (int64_t *) ptr);
			break;
		case CPORTH_OP_STORE8:
			ptr = POPPTR();
			*((int8_t *) ptr) = (int8_t) (POPINT() & 0xff);
			break;
		case CPORTH_OP_STORE16:
			ptr = POPPTR();
			*((int16_t *) ptr) = (int16_t) (POPINT() & 0xffff);
			break;
		case CPORTH_OP_STORE32:
			ptr = POPPTR();
			*((int32_t *) ptr) = (int32_t) (POPINT() & 0xffffffff);
			break;
		case CPORTH_OP_STORE64:
			ptr = POPPTR();
			*((int64_t *) ptr) = POPINT();
			break;
		case CPORTH_OP_CAST_PTR:
		case CPORTH_OP_CAST_INT:
		case CPORTH_OP_CAST_BOOL:
			break;
		case CPORTH_OP_ARGC:
			PUSHINT(args->argc);
			break;
		case CPORTH_OP_ARGV:
			PUSHPTR((void *) args->argv);
			break;
		case CPORTH_OP_ENVP:
			PUSHPTR((void *) environ);
			break;
		case CPORTH_OP_SYSCALL0:
			a = POPINT();
			PUSHINT(syscall(a));
			break;
		case CPORTH_OP_SYSCALL1:
			a = POPINT();
			b = POPINT();
			PUSHINT(syscall(a, b));
			break;
		case CPORTH_OP_SYSCALL2:
			a = POPINT();
			b = POPINT();
			c = POPINT();
			PUSHINT(syscall(a, b, c));
			break;
		case CPORTH_OP_SYSCALL3:
			a = POPINT();
			b = POPINT();
			c = POPINT();
			d = POPINT();
			PUSHINT(syscall(a, b, c, d));
			break;
		case CPORTH_OP_SYSCALL4:
			a = POPINT();
			b = POPINT();
			c = POPINT();
			d = POPINT();
			e = POPINT();
			PUSHINT(syscall(a, b, c, d, e));
			break;
		case CPORTH_OP_SYSCALL5:
			a = POPINT();
			b = POPINT();
			c = POPINT();
			d = POPINT();
			e = POPINT();
			f = POPINT();
			PUSHINT(syscall(a, b, c, d, e, f));
			break;
		case CPORTH_OP_SYSCALL6:
			a = POPINT();
			b = POPINT();
			c = POPINT();
			d = POPINT();
			e = POPINT();
			f = POPINT();
			g = POPINT();
			PUSHINT(syscall(a, b, c, d, e, f, g));
			break;
		case CPORTH_OP_OFFSET:
			assert(scan_ctx != NULL);
			a = POPINT();
			PUSHINT(scan_ctx->offset);
			scan_ctx->offset += a;
			break;
		case CPORTH_OP_RESET:
			assert(scan_ctx != NULL);
			PUSHINT(scan_ctx->offset);
			scan_ctx->offset = 0;
			break;
		case CPORTH_OP_DBG_STACKDUMP:
			break;
		case CPORTH_OP_NUMBER_OF:
			assert(0);
	};

	return ret;
}

static int __cporth_run_at (
		const cporth_token **token,
		const cporth_program *program,
		cporth_scan_ctx *scan_ctx,
		cporth_run_stack *stack,
		cporth_mem *mem
);

static int __cporth_run_keyword (
		const cporth_token **token,
		cporth_run_stack *stack
) {
	int ret = CPORTH_OK;

	const cporth_token *jump = NULL;

	again:

	jump = CPORTH_TOKEN_JUMPOF(*token); 

	switch ((*token)->kw->id) {
		case CPORTH_KW_IF:
		case CPORTH_KW_IFSTAR:
		case CPORTH_KW_DO:
			if (!POPINT()) {
				*token = jump;
			}
			break;
		case CPORTH_KW_SP_ORELSE:
		case CPORTH_KW_ELSE:
			assert((*token)->jump.i != 0);
			*token = jump;
			// Start after end
			break;
		case CPORTH_KW_WHILE:
		case CPORTH_KW_HERE:
			break;
		case CPORTH_KW_MACRO:
		case CPORTH_KW_CONST:
		case CPORTH_KW_ASSERT:
		case CPORTH_KW_CP_STRUCT:
			// Jump over struct definition or past instantiation name
			*token = (CPORTH_TOKEN_JUMPOF(*token) != NULL ? CPORTH_TOKEN_JUMPOF(*token) : (*token) + 1);
			break;
		case CPORTH_KW_PROC:
			// Jump to IN then END
			*token = CPORTH_TOKEN_JUMPOF(*token);
			*token = CPORTH_TOKEN_JUMPOF(*token);
			break;
		case CPORTH_KW_MEMORY:
			*token = CPORTH_TOKEN_JUMPOF_OR_SELF(*token);
			break;
		case CPORTH_KW_INCLUDE:
			(*token)++;
			break;
		case CPORTH_KW_END:
			if (jump != NULL) {
				*token = jump;
				goto again;
			}
			if (CPORTH_TOKEN_RETURNOF(*token)) {
				ret = CPORTH_END;
			}
			break;
		case CPORTH_KW_CP_VAR:
		case CPORTH_KW_INT:
		case CPORTH_KW_BOOL:
		case CPORTH_KW_PTR:
		case CPORTH_KW_BIKESHEDDER:
		case CPORTH_KW_IN:
			assert(0);
	};

	return ret;
}

static int __cporth_run_at (
		const cporth_token **token,
		const cporth_program *program,
		cporth_scan_ctx *scan_ctx,
		cporth_run_stack *stack,
		cporth_mem *mem
) {
	int ret = CPORTH_OK;

	const cporth_token *end = program->tokens + program->tokens_length;
	const cporth_token *token_jmp = NULL;
	for (; (*token) < end; (*token)++) {
		//printf("Run at %s type %i stack %lu\n", (*token)->ptr, (*token)->type, stack->length);
		switch ((*token)->type) {
			case CPORTH_TP_CALL:
				token_jmp = CPORTH_TOKEN_JUMPOF(*token);
				token_jmp = CPORTH_TOKEN_JUMPOF(token_jmp);
				token_jmp++;
				if ((ret = __cporth_run_at (
						&token_jmp,
						program,
						scan_ctx,
						stack,
						mem
				) & ~(CPORTH_END)) != 0) {
					goto out;
				}
				assert(token_jmp->kw != NULL && token_jmp->kw->id == CPORTH_KW_END);
				// Continue after jump
				break;
			case CPORTH_TP_WORD:
				if ((ret = __cporth_run_word (&(*token), scan_ctx, stack, mem)) != 0) {
					goto out;
				}
				break;
			case CPORTH_TP_INT:
			case CPORTH_TP_BOOL:
			case CPORTH_TP_PTR:
				PUSHINT(* (int64_t *) (*token)->ptr);
				break;
			case CPORTH_TP_STR:
				PUSHINT((*token)->size);
				PUSHPTR((*token)->ptr);
				break;
			case CPORTH_TP_CSTR:
				PUSHPTR((*token)->ptr);
				break;
			case CPORTH_TP_CHAR:
				PUSHINT(* (uint8_t *) (*token)->ptr);
				break;
			case CPORTH_TP_KEYWORD:
				if ((ret = __cporth_run_keyword (token, stack)) != 0) {
					goto out;
				}
				break;
		};
	}

	out:
	return ret;
}

static int __cporth_run (
		const cporth_program *program,
		cporth_run_stack *stack,
		cporth_mem *mem
) {
	const cporth_token *token = program->tokens;

	return __cporth_run_at (
			&token,
			program,
			NULL,
			stack,
			mem
	);
}

int cporth_run (
		const cporth_program *program,
		int argc,
		const char **argv
) {
	int ret = CPORTH_OK;

	cporth_run_stack *stack = NULL;
	cporth_mem mem = {0};

	if ((ret = __cporth_run_stack_new (&stack, program->tokens_length + CPORTH_RUN_STACK_EXTRA)) != 0) {
		goto out;
	}

	// Args structure is always first in the memory
	if ((ret = __cporth_run_allocate_args(&mem, argc, argv)) != 0) {
		goto out_free;
	}

	if ((ret = __cporth_run (program, stack, &mem)) != 0) {
		goto out_free;
	}

	if (ret == 0 && stack->length != 0) {
		cporth_err("Stack not empty at program exit despite non-erronous return value (contains %lu elements)\n", stack->length);
		ret = CPORTH_ERR_HARD;
		goto out_free;
	}

	out_free:
		__cporth_run_stack_destroy(stack);
	out:
		cporth_mem_cleanup(&mem);
		return ret;
}

int cporth_run_partial (
		const cporth_program *program,
		cporth_scan_ctx *scan_ctx,
		const cporth_token *begin,
		int (*callback)(int64_t data, void *arg),
		void *arg
) {
	assert(scan_ctx != NULL);

	int ret = CPORTH_OK;

	cporth_run_stack *stack = NULL;
	cporth_mem mem = {0};

	if ((ret = __cporth_run_stack_new (&stack, program->tokens_length + CPORTH_RUN_STACK_EXTRA)) != 0) {
		goto out;
	}

	const cporth_token *t = begin;
	if ((ret = __cporth_run_at (
			&t,
			program,
			scan_ctx,
			stack,
			&mem
	)) != 0 && ret != CPORTH_END) {
		goto out_free;
	}

	assert (stack->length == 1);

	if ((ret = callback(stack->frames[0].data, arg)) != 0) {
		goto out_free;
	}

	out_free:
		__cporth_run_stack_destroy(stack);
	out:
		cporth_mem_cleanup(&mem);
		return ret;
}

