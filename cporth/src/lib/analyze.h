#ifndef CPORTH_ANALYZE_H
#define CPORTH_ANALYZE_H

#include <sys/types.h>

#include "analyze_data.h"

typedef struct cporth_program cporth_program;
typedef struct cporth_scan_ctx cporth_scan_ctx;
typedef struct cporth_token cporth_token;

int cporth_analyze_partial (
		size_t *stack_length,
		const cporth_program *program,
		const cporth_scan_ctx *scan_ctx,
		const cporth_token *begin,
		const cporth_token *end,
		int (*callback)(cporth_analyze_data data, void *arg),
		void *arg
);
int cporth_analyze (
		const cporth_program *program
);

#endif /* CPORTH_ANALYZE_H */
