#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#include "allocate.h"
#include "io.h"
#include "error.h"
#include "log.h"
#include "posix.h"
#include "util.h"

int cporth_io_read_file (
		void **data,
		size_t *length,
		const char *filename
) {
	int ret = CPORTH_OK;

	*data = NULL;
	*length = 0;

	int fd = 0;
	off_t off = 0;
	char *data_tmp = NULL;

	if ((fd = open (filename, 0)) < 0) {
		cporth_err("Failed to open file %s: %s\n", filename, cporth_strerror(errno));
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	if ((off = lseek (fd, 0, SEEK_END)) < 0) {
		cporth_err("Failed to seek to the end of file %s: %s\n", filename, cporth_strerror(errno));
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	if (lseek (fd, 0, SEEK_SET) != 0) {
		cporth_err("Failed to seek to the beginning of file %s: %s\n", filename, cporth_strerror(errno));
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	if ((data_tmp = cporth_allocate((size_t) off + 1)) == NULL) {
		cporth_err("Failed to allocate %lu bytes in %s\n", (size_t) off + 1, __func__);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	ssize_t read_bytes = 0;

	if ((read_bytes = read(fd, data_tmp, (ssize_t) off)) != off) {
		cporth_err("Failed to read %li bytes from file %s: %s\n", (ssize_t) off, filename, cporth_strerror(errno));
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	*data = data_tmp;
	*length = (size_t) read_bytes;
	data_tmp = NULL;

	out:
	CPORTH_FREE_IF_NOT_NULL(data_tmp);
	return ret;
}
