#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>

#include "allocate.h"
#include "util.h"

int cporth_vasprintf (char **resultp, const char *format, va_list args) {
	int ret = 0;

	size_t size = strlen(format) * 2;
	char *buf = NULL;

	*resultp = NULL;

	retry:
	CPORTH_FREE_IF_NOT_NULL(buf);
	if ((buf = cporth_allocate(size)) == NULL) {
		ret = -1;
		goto out;
	}

	int retry_count = 0;

	va_list args_tmp;
	va_copy(args_tmp, args);
	ret = vsnprintf(buf, size, format, args_tmp);
	va_end(args_tmp);

	if (ret < 0) {
		ret = -1;
		goto out;
	}
	else if ((size_t) ret >= size) {
		if (++retry_count > 1) {
			ret = -1;
			goto out;
		}
		size = (size_t) ret + 1;
		goto retry;
	}

	ret = (int) strlen(buf);

	*resultp = buf;
	buf = NULL;

	out:
	CPORTH_FREE_IF_NOT_NULL(buf);
	return ret;
}

int cporth_asprintf (char **resultp, const char *format, ...) {
	int ret = 0;
	va_list args;
	va_start (args, format);

	ret = cporth_vasprintf(resultp, format, args);

	va_end (args);

	return ret;
}
