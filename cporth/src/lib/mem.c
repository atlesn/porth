#include <stddef.h>
#include <string.h>
#include <assert.h>

#include "mem.h"
#include "allocate.h"
#include "error.h"
#include "log.h"
#include "error.h"

static int __cporth_mem_allocate (
		cporth_mem *mem,
		size_t size
) {
	int ret = CPORTH_OK;

	if ((mem->data = cporth_allocate(size)) == NULL) {
		cporth_err("Failed to allocate memory in %s\n", __func__);
		ret = CPORTH_ERR_HARD;
		goto out;
	}

	mem->size = size;

	out:
	return ret;
}

void *cporth_mem_get (
		cporth_mem *mem,
		size_t size
) {
	void *ret = mem->data + mem->pos;
	mem->pos += size;

	return ret;
}

int cporth_mem_init (
		cporth_mem *mem,
		size_t size
) {
	assert(mem->data == NULL);
	return __cporth_mem_allocate(mem, size);
}

void cporth_mem_cleanup (
		cporth_mem *mem
) {
	if (mem->data != NULL)
		cporth_free(mem->data);
	memset(mem, '\0', sizeof(*mem));
}
