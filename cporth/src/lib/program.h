#ifndef CPORTH_PROGRAM_H
#define CPORTH_PROGRAM_H

#include "token.h"
#include "location.h"

typedef struct cporth_program cporth_program;
typedef struct cporth_token_ref cporth_token_ref;

#define CPORTH_PROGRAM_KW_POSTPROCESS_ARGS                     \
    cporth_token *token,                                       \
    const cporth_keyword *kw,                                  \
    void *arg

int cporth_program_new (
		cporth_program **program
);
void cporth_program_destroy (
		cporth_program *program
);
void cporth_program_tokens_trim (
		cporth_program *program,
		cporth_token_ref last
);
int cporth_program_token_push (
		cporth_program *program,
		cporth_token_type token_type,
		const void *data,
		size_t size,
		const cporth_location loc,
		int (*postprocess)(CPORTH_PROGRAM_KW_POSTPROCESS_ARGS),
		const cporth_keyword *kw,
		void *arg
);
int cporth_program_token_copy_and_push (
		cporth_program *program,
		const cporth_token_ref tref,
		const cporth_location *loc
);
void cporth_program_dump (
		const cporth_program *program
);
int cporth_program_here_str_l (
		char **result,
		const cporth_location *loc
);
void cporth_program_err_custom_l (
		const cporth_location *loc,
		const char *str
);
void cporth_program_err_l (
		const cporth_location *t
);
void cporth_program_err_custom (
		const cporth_program *program,
		const cporth_location_ref lref,
		const char *str
);
void cporth_program_err (
		const cporth_program *program,
		const cporth_location_ref lref
);
void cporth_program_err_descend (
		const cporth_program *program,
		const cporth_location_ref lref
);
void cporth_program_err_custom_t (
		const cporth_program *program,
		const cporth_token *token,
		const char *str
);
void cporth_program_err_t (
		const cporth_program *program,
		const cporth_token *token
);
void cporth_program_err_descend_t (
		const cporth_program *program,
		const cporth_token *token
);
size_t cporth_program_nesting_count (
		const cporth_program *program,
		const cporth_location_ref lref
);

#endif /* CPORTH_PROGRAM_H */
