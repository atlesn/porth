#ifndef CPORTH_ALLOCATE_H
#define CPORTH_ALLOCATE_H

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

static inline void *cporth_allocate (size_t size) {
	return malloc(size);
}

static inline void *cporth_allocate_zero (size_t size) {
	return calloc(1, size);
}

static inline void *cporth_reallocate (void *ptr, size_t size) {
	return (ptr == NULL ? malloc(size) : realloc(ptr, size));
}

static inline char *cporth_strdup (const char *str) {
	const size_t size = strlen(str) + 1;
	char *res = malloc(size);
	memcpy(res, str, size);
	return res;
}

static inline void cporth_free (void *ptr) {
	free(ptr);
}

int cporth_vasprintf (char **resultp, const char *format, va_list args);
int cporth_asprintf (char **resultp, const char *format, ...);

#undef malloc
#define malloc(...) __intercepted_illegal_malloc

#undef calloc
#define calloc(...) __intercepted_illegal_calloc

#undef realloc
#define realloc(...) __intercepted_illegal_realloc

#undef strdup
#define strdup(...) __intercepted_illegal_strdup

#undef free
#define free(...) __intercepted_illegal_free

#undef vasprintf
#define vasprintf(...) __intercepted_illegal_vasprintf

#undef asprintf
#define asprintf(...) __intercepted_illegal_asprintf

#endif /* CPORTH_ALLOCATE_H */
