#ifndef PORTH_PARSER_H
#define PORTH_PARSER_H

typedef struct cporth_program cporth_program;
typedef struct cporth_location cporth_location;

int cporth_scan_file_to_program (
		cporth_program *program,
		const char *filename
);
int cporth_scan_init (void);
void cporth_scan_cleanup (void);

#endif
