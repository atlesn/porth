#ifndef CPORTH_SCAN_CTX_H
#define CPORTH_SCAN_CTX_H

#include <inttypes.h>

typedef struct cporth_dict cporth_dict;

struct cporth_scan_ctx {
	int64_t offset;
	cporth_dict *dict;
};

#endif
