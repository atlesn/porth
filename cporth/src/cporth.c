#include <stdlib.h>
#include <stdio.h>

#include "lib/cporth.h"

static void usage(const char *prog) {
	cporth_err("Usage: %s {filename} [argument]...\n", prog);
}

struct program_callback_data {
	int argc;
	const char **argv;
};

int program_callback (
		const cporth_program *program,
		void *arg
) {
	struct program_callback_data *callback_data = arg;

	int ret = CPORTH_OK;

	// cporth_program_dump(program);

	if ((ret = cporth_analyze(program) & ~CPORTH_EOF) != 0) {
		goto out;
	}

	if ((ret = cporth_run(program, callback_data->argc - 1, callback_data->argv + 1)) != 0) {
		goto out;
	}

	// Any future compilation goes here

	out:
	return ret;
}

int main(int argc, const char **argv) {
	int ret = EXIT_SUCCESS;

	// TODO : Add interactive mode (no filename)

	if (argc < 2) {
		usage(argv[0]);
		ret = EXIT_FAILURE;
		goto out;
	}

	struct program_callback_data callback_data = {
		argc,
		argv
	};

	if ((ret = cporth_main_file_to_program(argv[1], program_callback, &callback_data)) != 0) {
		goto out;
	}

	out:
	return ret;
}
